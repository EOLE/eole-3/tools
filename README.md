# Outil d'aide au déploiement des services Eole³

## Présentation de l'outil

L'outil [eole3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/) sert à générer les différents fichiers nécessaires au déploiement des charts pour les services **Eole³**.
La procédure d'utilisation est la suivante :

- Adapter les valeurs par défaut à votre configuration
- Générer les fichiers indispensables
- Déployer la configuration

## Prérequis

Il vous faut disposer :

- d'un cluster Kubernetes
- du fichier d'accès à l'API Kubernetes :`export KUBECONFIG=kubeconfig.yaml`
- de la commande [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- de la commande [helm](https://helm.sh/docs/intro/install/)
- de la commande [jq](https://stedolan.github.io/jq/)
- d'un nom de domaine
- d'un serveur DNS qui résoud l'enregistrement wilcard pour le domaine
- d'un certificat wildcard reconnu pour ce domaine. Il est impossible d'utiliser un certificat auto-signé

## Description du socle de base

Les différents services applicatifs inclus dans la base sont listés dans le tableau ci-dessous. La boîte est le service principal, toujours déployé, les autres services sont optionnels et activables au moment de la configuration.

| Applications  | Description                                |
| ------------- | ------------------------------------------ |
| Laboite       | Portail d'accès aux services Eole³         |
| Agenda        | Agenda partagé                             |
| Blog          | Affichage des articles de blog             |
| Blogapi       | API REST d'accès aux articles de blog      |
| Frontnxt      | Aiguilleur d'accès aux nuages (Nextcloud©) |
| Lookup-server | Moteur de recherche des utilisateurs       |
| Mezig         | Publication des compétences                |
| Questionnaire | Création et gestion de questionnaires      |
| Radicale      | Serveur de calendrier (caldav)             |
| Sondage       | Création et gestion de sondages            |

Les entrées du tableau ci-dessous correspondent aux services d'infrastructure nécessaires à l'exploitations des applications précédentes (ils sont toujours déployés).

| Nom            | Description                                                       |
| -------------- | ----------------------------------------------------------------- |
| cert-manager   | Gestion des certificats Let's Encrypt                             |
| Keycloak       | Serveur d'authentification unique                                 |
| Ingress-NGINX  | Contrôleur ingress                                                |
| Mariadb        | Serveur de bases de données                                       |
| Mongodb        | Base de données en mode replicaset                                |
| Minio          | Serveur de stockage S3                                            |
| Nginx          | Contrôleur ingress par défaut                                     |
| PostgreSQL     | Serveur de bases de données non HA                                |
| Redis          | Serveur de base clefs/valeurs                                     |

## Les services additionnels

Grâce à cet outil, certaines applications peuvent être intégrées au socle de base. Cette liste est destinée à s'étoffer.

| Nom            | Description                          | SSO                   | Statut |
| -------------- | ------------------------------------ | --------------------- | -------|
| CodiMD         | Serveur d'édition collaborative      | configuré             | stable |
| Collabora      | Suite office en ligne                | sans authentification | stable |
| Discourse      | Serveur de forums et discussion      | manuel                | beta   |
| Drawio         | Outil de diagramme en ligne          | sans authentification | stable |
| Element-web    | Client de messagerie instantannée    | configuré             | stable |
| Excalidraw     | Outil de dessin collaboratif         | sans authentification | beta   |
| Filepizza      | Transfert de fichiers                | sans authentification | beta   |
| HedgeDoc       | Serveur d'édition collaborative      | configuré             | beta   |
| Mastodon       | Réseau social décentralisé           | configuré             | beta   |
| Matomo         | Serveur de mesure de statistiques web| configuré             | beta   |
| Mobilizon      | Gestion d'évènements communautaire   | configuré             | beta   |
| Nextcloud      | Partage de documents dans les nuages | configuré             | stable |
| Screego        | Partage d'écran                      | sans authentification | beta   |
| Shlink         | Serveur raccourcisseur d'URL         | sans authentification | stable |
| Synapse        | Serveur de messagerie instantanée    | configuré             | beta   |
| Wikijs         | Plateforme de Wiki                   | manuel                | stable |

## Les services Ladigitale

Des services du projet Ladigitale sont déployables à l'aide de cet outil.

| Nom            | Description                          | SSO                   | Statut |
| -------------- | ------------------------------------ | --------------------- | -------|
| Digibunch      | Création de bouquets de liens        | sans authentification | stable |
| Digiflashcards | Création de cartes mémos             | sans authentification | stable |
| Digimindmap    | Création de cartes heuristiques      | sans authentification | stable |
| Digiquiz       | Lire et partager des contenus H5P    | sans authentification | stable |
| Digisteps      | Création de parcours pédagogiques    | sans authentification | stable |
| Digistrip      | Création de bandes dessinées         | sans authentification | stable |
| Digiview       | Visionner de vidéos Youtube sans pub | sans authentification | stable |
| Digiwords      | Création de nuages de mots           | sans authentification | stable |

## Les outils d'administration

Cet outil permet également d'intégrer des outils d'administration. Cette liste est destinée à s'étoffer.

| Nom                   | Description                                             | Status |
| ----------------------| ------------------------------------------------------- | ------ |
| Kubernetes-dashboard  | Tableau de bord Kubernetes                              | stable |
| Loki-stack            | Visualiseur de logs et exporteur de logs                | stable |
| node-problem-detector | Supervision de nœuds k8s                                | stable |
| Prometheus-stack      | Collecteur de métriques (prometheus-operator + grafana) | stable |
| Promtail              | Collecteur de logs                                      | stable |
| SuperCRUD             | UI d'administration de la base mongodb laboite          | stable |


[Utilisation de l'outil](GETTING_STARTED.md)
