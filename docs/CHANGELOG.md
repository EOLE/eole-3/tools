# Changelog

## [10.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/9.0.0...release/10.0.0) (2025-01-31)


### ⚠ BREAKING CHANGES

* **keycloak:** user parameters in admin subsection has a different behaviour
- user value is moved to the new tmpUser parameter
- password is moved to the new tmpPassword parameter
- user becomes the permanent admin username
- password becomes the permanent admin password

Use user/password parameters to connect the the admin console
* **keycloak:** admin user parameters moved to admin subsection
  - adminUser becomes admin.user
  - adminPassword becomes admin.password

### Features

* **addons:** add maintenance script ([c5c85c3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c5c85c393fb0be6bca0e2778f17a2ad933d3ddb2))
* **admin-tools:** add maintenance script ([2b882ac](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2b882ac42be90eca8f2745d1c2d10df4a1ca4f3e))
* **CI:** add CI for maintenance component ([40a92ba](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/40a92bab854a25f2ff7c1cd81d5cb144e305cf7f))
* **eole3:** add maintenance command ([0044f25](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0044f253b08c5025f8934220c710934457d97c46))
* **eole3:** deploy socle can deploy laboite services only ([7776c56](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7776c56eb7524120bba1eda30b990102e7cc712d))
* **infra:** add maintenance helm chart ([c0ccd24](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c0ccd242edc2cd379a4f2a6b2cb4f84a377d60c4))
* **infra:** add maintenance script ([cfe17ed](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cfe17ed0860e9c56be5a093552bf0f53847e51ff))
* **keycloak:** reorganise admin section ([99603bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/99603bfc33f77ca4bd1d1ff71a45511cdb1582fd))
* **ladigitale:** add maintenance script ([92e2c52](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/92e2c52d739450e77852a87ca647cc60071ca458))
* **socle:** add maintenance script ([9fb0587](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9fb058700e44c8ba696235871f6f15d362fabec0))
* update backup and restore documentation ([3a4a799](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3a4a799093b0ddd328746e169ccdd73ffc8dec4a))
* update drawio helm version to 1.3.0 ([149da02](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/149da028928cb9c7117acc64747a6c25b4577834))


### Bug Fixes

* **admin-tools:** correct ingress name for grafana ([877450a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/877450ab492609375406747651c36583269223ac))
* **codimd:** split repository and registry ([aaee450](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/aaee4509ea381125a1310188b53b522f3dbca6c6))
* **eole3:** maintenance script is now a macro ([24ec187](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/24ec187cb75bbe63dc359659df017256d789d5cb))
* **keycloak:** create a permanent admin acccount ([04d2228](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/04d2228a1fdd9ffa8ec176f7c1c89b1f97eb87ad))
* **keycloak:** deploy keycloak 26.0.8-eole3.102 by default ([7b65760](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7b65760a1fbd06c267a2460bd2ff0da4937c6ff0))
* **keycloak:** remove useless pubkey parameter ([9446537](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/944653783bc7333476dea75591e6ace17812f384))
* **keycloak:** set correctly the permanent admin account password ([0a68945](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0a68945703f1a19339e5c2b7a835eab6f788344e))
* **mastodon:** use keycloak macro in order to create client ([dd0a912](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dd0a912b87ae5034e92c74f2da6a908631a9ddd4))
* **postgresql:** postgresql can be restored in multiple server versions ([911a058](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/911a058e6f2d150b031a19e26c14534f8304229c))
* **socle:** deploy laboite helm version 1.12.4 ([2d0150b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2d0150b9a88f2095cb71b3d32210102716c94743))

## [9.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/8.1.1...release/9.0.0) (2024-12-02)


### ⚠ BREAKING CHANGES

* **socle:** component section contains an images subsection
  images:
    registry: ["~","upstream","<your registry url>"]
    ~ is default and mean hub.eole.education
    upstream is default helm values
    <image_name>:
      name: <image full path on registry>
      tag: <image tag>
    ...

Example for postgresql:
  images:
    registry: ~
    postgresql:
      name: eole3/postgresql-container-image/postgresql
      tag: 16.2.0-debian-12-r14-eole3.0
    os-shell:
      name: eole3/os-shell-container-image/os-shell
      tag: 16.2.0-debian-12-r14-eole3.0
    postgres-exporter:
      name: eole3/postgresql-exporter-container-image/postgres-exporter
      tag: 16.2.0-debian-12-r14-eole3.0

### Features

* **addons:** refactor container images parameters ([1fbf7f2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1fbf7f237a056d2ee80732083c54c601caf8b4e4))
* **admin-tools:** refactor container images parameters ([01da627](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/01da627bca70af747454f5f2f4d5203e8dc5ad6b))
* **eole3:** remove useless container image macro ([7490d93](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7490d93b08eb9c72f6caeb5e525ac87b585a8f6c))
* **keycloak:** update keycloak to version 25 ([d8d2735](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d8d27358341b096a586c6c439c900b1c55524543))
* **ladigitale:** refactor container images parameters ([76a76b7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/76a76b7873929bf3ee3abf81c1e8ae20f9b58cf6))
* **nextcloud:** custom container image is available ([0eda0ee](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0eda0ee0ae834fc6ecf0b8479919a68b5d79b1d9))
* **nextcloud:** permit to use external redis ([602dc19](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/602dc190fe8e0a68df5435f44d2145446eaed2e7))
* **socle:** refactor container images parameters ([8a15bd0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8a15bd0e2803686c38ca8f24345121abb452423b))


### Bug Fixes

* corrections on jinja2 syntax ([f20eb27](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f20eb27bb5284546508abdf72d1b4b8bd36106bc))
* **eole3:** check if tag exists in values helm ([3b8b249](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3b8b249d46c73f0d2f4983b479d33fb226279da9))
* **keycloak:** activate keycloak metrics events if it is enabled ([d195bdf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d195bdf0b1843180bf98ceee81a1c1f49403d628))
* **keycloak:** add missing features ([7ab5c91](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7ab5c915968cc34b1fc336d5cffe518041d9d61c))
* **keycloak:** configure realm locale settings only if required ([37a40f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/37a40f497ac5a3e60b65a9b48fc448f3b4c7c676))
* **keycloak:** deploy version 25.0.6-eole3.86 with arguments ([0c73698](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0c73698078567605671ba2f5def96989a19086d1))
* **keycloak:** enable user and admin events for laboite realm ([9db59ae](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9db59ae0d0d9fa2660fe20d2539389c8f30318aa))
* **laboite:** adjust values in meteor_settings ([5ac11f8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5ac11f8034bb0b79a80bf746d1f3d45901e582cf))
* **laboiteapi:** correct images section in vars.yaml ([2dd0c7b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2dd0c7b11d5d058d48302518c99bf8e7c0e30a69))
* **laboite:** update default values in meteor_settings ([978d4da](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/978d4da0101efea11b83249f6996001949bef078))
* **nextcloud:** need to upgrade nextcloud to avoid crashloopbackoff ([bf213df](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bf213df96a1482c6d2c97845da86402f822245ea))
* **nextcloud:** use hostname as pod label name ([7136f46](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7136f4657c6323f17433d59fd4cb00d088279f31))
* **radicale:** support mongodb complex passwords ([78037a1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/78037a16f7709d547f5966b7dae33480fb9dc4dd))
* **socle:** create/update KC admin api user with mandatory parameters ([2b60b16](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2b60b1640b705d3480cfbe7a1d8a20624ca7744d))
* **synapse:** add registry entry ([6abcb84](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6abcb847152cbda9e1ccf42f36da8d9372b2043c))

### [8.1.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/8.1.0...release/8.1.1) (2024-11-25)


### Bug Fixes

* **mongodb:** mongodb resources can be modified ([d1cbc87](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d1cbc87dd6b6d7fbd44509ccc882cd6148acb1c8))

## [8.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/8.0.0...release/8.1.0) (2024-10-25)


### Features

* **keycloak:** admin console can be restricted to reserved IPs ([cb918bd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cb918bd7eb03d11cf3617612d0c2c79514322176))


### Bug Fixes

* **codimd:** update helm chart to 1.2.1 ([281aaf1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/281aaf1ba89154fb7f4dec3a2da83b15d6a4349c))
* **element:** add image tag support ([b30429f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b30429f278503be8908ffdcc05a7c78ba25e4319))
* **element:** deploy element helm chart version 1.2.1 ([bb00758](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bb00758c179bee67f6b064be4e50b05737869561))
* **laboite:** deploy helm chart 1.12.3 with appversion 6.0.1 ([e6daa9f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e6daa9fcc4cdc51dad0f85d384233b05a11149c9))
* **synapse:** deploy helm version 1.2.1 ([8f57166](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8f57166088106a31c0222c9d299c0eee64da6197))

## [8.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/7.0.5...release/8.0.0) (2024-10-15)


### ⚠ BREAKING CHANGES

* **socle:**     s3.backup.enabled = false completly deactivate s3 backup
    s3.backup.schedule = false deactivate s3 backup schedule
* **socle:**   database.backup.enabled = false completly deactivate database backup
  database.backup.schedule = false deactivate database backup schedule
* **mongodb:** Rename and reorganise mongodb parameters in vars.yaml
   - Mongodb infra component section:
       - mongoName become hostname
       - mongoPort become port
       - mongoRsname become rsName
       - mongoRootPassword become a new sub-section admin with password
         parameter
       - mongoDatabase, mongoUsername and mongoPassword are moved to
         a database section in each service needing mongodb database
     No database is created during mongodb deployment. Each service
using mongodb provider will create its own database.

   - Laboite component section gain new database section with:
       database:
          - provider: mongodb
          - dbname (old mongoDatabase parameter)
          - user (old mongoUsername parameter)
          - password (old mongoPassword parameter)

### Features

* **cert-manager:** update helm chart version to v1.15.3 ([c77192a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c77192a9024d1ef99072f1f5195559b640834025))
* **ci:** add last added ladigitale apps ([b731260](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b7312607b92c736b0c47913e54afa83d0dd3c7fa))
* **collabora:** admin console external access can be blocked ([42dc043](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/42dc043c171caea530ecb1f427b93801dd1d6b9c))
* **collabora:** restrict access to uris list ([4f3bac0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4f3bac050e00807d93597da9bed347f298c69c71))
* **eole3:** add command to list PostgreSQL backup ([6b4c4f9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6b4c4f9a7ad444e954b9fb5a8bec3e50d518551b))
* **eole3:** add save and restore actions ([f300469](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f3004694e8b55f324e4e5b8d9eb53241016bf841))
* **eole3:** split backup and restore command ([46fe18f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/46fe18f8f2555698fa02138c9f22bd801c286117))
* **ingress-nginx:** deploy chart 4.11.2 with eole3 container image ([ad83c66](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ad83c667faefaa25072ac73c89336db529977ff2))
* **ladigitale:** add some apps to ladigitale ([61c789b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/61c789ba4349109ce286432d9e39ccd2703e1e38))
* **mongodb:** factorise resources and scripts ([b1461ce](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b1461ceba38086c61017f65690b1d6b91cca147a))
* **mongodb:** update mongodb helm chart to 15.6.26 ([3a7c32d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3a7c32d7bbaa8f2e434a65deefb0871e1a828729))
* **s3:** backup command can list s3 backups ([19acde5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/19acde578be2f4daf8897c1b1fbfdc12b8ef7893))
* **screego:** deploy screego 1.10.5-eole3.0 from hub.eole.education ([ff65f87](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ff65f878f5a6a4e0781c9c600d1104eaf570b686))
* **socle:** database backup can be deactivated ([7eef075](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7eef075e73a7f6b8d5ebb7bd4991c2231a2f8dea))
* **socle:** s3 backup can be deactivated ([dd319f0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dd319f0aea55847b2a59a4df09d0a35a2dcb8b20))
* update helm chart to 1.0.0-dev.3 ([f4b1c65](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f4b1c65c4c222e630bfc1c2933270ee756b691de))


### Bug Fixes

* **backup:** use job instead of cronjob to make a mariadb backup ([ac1c20b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ac1c20b14d0568f8aeb7ccc1466b4afd7d3da68b))
* **backup:** use job instead of cronjob to make a mongodb backup ([f183911](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f1839118256eee4a3d609d7e02f5feb7a60db576))
* **backup:** use job instead of cronjob to make a postgresql backup ([9c6f92f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9c6f92f783125b4d94f644d0db84197cd6f998a7))
* **backup:** use job instead of cronjob to make a s3 backup ([03b15c8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/03b15c88cd4752943074d1d6ec27af1f0a1676a0))
* build -o display good deploy command ([aed2f5f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/aed2f5f446d95f78b6b3d2416f3e6b339cc2bcac))
* **ci:** temporary disable node-problem-detector ([fc212dc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fc212dc4b10fecde69da87b6db56a31ea3cdbdba))
* **codimd:** customise client body max size ([1c583b5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1c583b54e3c8dd5be5f6133ef47dfb3f11fa7484))
* **collabora:** use eole images and helm chart ([06695ee](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/06695ee090daddcb7a77ecd15f6a6546ed888151))
* **doc:** update components list ([bef3efe](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bef3efeab86f4e331a634aa5f06a5cbfdf27386e))
* **eole3:** don't print the traceback on deploy command error ([e458866](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e458866ef2ebda95e0b7fea569b93affee7b8fb8))
* **eole3:** pull stable tag for other images from hub.eole.education ([7c8050b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7c8050b3e50231db385f0202d1e34e4c0b517bc5))
* **infra:** postgresql port should always be a string ([13d72f7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/13d72f78d46d944130436fca0eefdd7f19d50820))
* **ingress-nginx:** correct the kube-webhook-certgen image section ([cf711d3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cf711d3415dd8fb04630518792e63c746ac1922a))
* **laboite:** deploy laboite chart 1.12.0 ([64ca390](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/64ca390229ce76e6ac4a1b87c279904350200079))
* **laboite:** redeploy laboite works if keycloak pubkey has changed ([4e7134a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4e7134a141ecb77a622eb99dcf805a2adb863957))
* **laboite:** update helm chart to 1.12.2 ([7bf4018](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7bf4018cda51deeb0a18d20d3f7c920b890d7821))
* **laboite:** update laboite helm chart to 1.12.1 ([caddf97](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/caddf97f63b7900802655d1433c65a18748b3849))
* **laboite:** use the same secret as redirection url ([52a74f7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/52a74f7ed286e97b5a84092df1c12a4e37dd1142))
* **ladigitale:** image parameters use the container image macro ([1c47cb8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1c47cb88d6c5f36c25b9cbf288735f5587bd861c))
* **ladigitale:** redis password must be quoted ([e3a8dbe](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e3a8dbec0009be6a28ac9f890d7ca7cbaf7f4621))
* **ladigitale:** redis password must be urlencoded ([800ca95](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/800ca955ef55c5443513aa477aacc1158e347055))
* **supercrud:** update supercrud helm version 1.2.1 ([e4d61f7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e4d61f7d2e8ee9a58af6ce3285098de7b3c3ff10))

### [7.0.5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/7.0.4...release/7.0.5) (2024-07-22)


### Bug Fixes

* **socle:** deploy laboite 1.11.3 ([1b9cf82](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1b9cf824d07e72102368d6928f9f34b9181e2126))

### [7.0.4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/7.0.3...release/7.0.4) (2024-07-09)


### Bug Fixes

* **laboite:** use latest hotfix 1.11.2 ([fb5323b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fb5323b80201f06208fb7134d05836a59753dd51))

### [7.0.3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/7.0.2...release/7.0.3) (2024-07-04)


### Bug Fixes

* **postgresql:** creation of database can take more than 30 seconds ([defb558](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/defb55803111af5f1518e2834c95c732a2ca2401))

### [7.0.2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/7.0.1...release/7.0.2) (2024-07-03)


### Bug Fixes

* **socle:** deploy keycloak image 22.0.5-eole3.12 by default ([e1ecef0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e1ecef01e2a9e99d9c96be7dde1ef53ac33c6cc8))

### [7.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/7.0.0...release/7.0.1) (2024-07-02)


### Bug Fixes

* **backup:** don't remove backup cronjob if schedule is enabled ([e0f2f8d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e0f2f8d0e502018dc86ba73ac73d86b747ca6541))

## [7.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/6.1.4...release/7.0.0) (2024-06-18)


### ⚠ BREAKING CHANGES

* **admin-tools:** each admin-tool must have its keycloak client
configuration
* **addons:** each addon must have its keycloak client
configuration
* **socle:** each component must have its keycloak client
configuration

### Features

* add custom image repository ([191e861](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/191e8617a44f709ee539fad8668298799b2c861b))
* **addon:** add element template as new addon ([45e568f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/45e568f6135b5e674ec2f2c45ed8de7dbe94202c))
* **addon:** add synapse template as new addon ([0869a52](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0869a52fcdf799f38102e3a54c78599017b46a1a))
* **addon:** deploy element-web chart 1.2.0 ([27db581](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/27db581d73a789bc015dd8a21efba2c2b0995511))
* **addon:** deploy supercrud helm chart 1.2.0 ([360991b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/360991b3c9eea81e2f32f013755a42239b9f57a2))
* **addon:** shlink supports mariadb ([fb4a7c9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fb4a7c93372a1e3b46d368c709bb515face782ce))
* **addons:** move keycloak client config to component section ([a6c87e3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a6c87e36a9ed4e44fef641225d99db9c62cad9b3))
* **admin-tools:** move keycloak client config to component section ([c74a5c9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c74a5c91789d5b8d45f30cbb334edf82eb088a1f))
* **cert-manager:** deploy chart v1.14.4 using hub eole image registry ([49390d6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/49390d66b2efeb5e30666ac74744e1e83584323a))
* **CI:** add ladigitale builds and tests ([ef2d6f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ef2d6f40ba2e24d8823407993ffe0689b235bc7b))
* **codimd:** deploy helm 1.1.0 with codimd 2.5.3 ([a8e2694](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a8e2694208d9fa450c9dc800c59a34ccad3f2bb7))
* **codimd:** support external database ([2665c0f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2665c0f57b9a01a9c7c6026ee95d6562a3041428))
* **command:** add show command to list components ([9fd448f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9fd448f6b05a75886f49e4be2f600a4309f134b6))
* **command:** document the show command ([e749dc3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e749dc33674e10191e121dd125710726054ddbc0))
* **database:** add additional options for postgresql database creation ([bb87e0d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bb87e0dd75b0c1dde174ad576aba872162dbdaa2))
* deploy laboite helm chart 1.11.1 ([4631658](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4631658591245618c35c4ed793db223aac8fb803))
* deploy new laboite helm chart version 1.11.0 ([6d0e964](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6d0e964c5fcbf6d1bb572ab63f74ab33129038d1))
* **digibunch:** add ladigitale digibunch first version ([1590b70](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1590b70a5ff47318729a0a2073af025de3618215))
* disable autoscaling ([62e7025](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/62e7025b3ab1a1484c17d886c5b97dcb819ac7b4))
* **element:** deploy helm 1.1.0 from eole3 registry ([d97d7e3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d97d7e3a2e6c11c3a02f3059bdbc3a4883ccd388))
* **eole3:** a single config file can be used ([7d4861f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7d4861f8f41188179364b03a645fbcf0fde33b90))
* **eole3:** commands complete component name ([7d289a0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7d289a0bdd4f8ce3bec81c5c2af80ef555a3c685))
* **eole3:** factorize component option ([f92310f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f92310fbe36897522d8650a43ffdf34c24e67c54))
* **infra:** initial add of mariadb infra ([fdf9794](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fdf9794ca9f46c4e32621b9d7a2780eff115ec77))
* **infra:** initial commit for redis component ([ca398c8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ca398c84f3dab4fb00ed719b46a674510ba2b74f))
* **ingress-nginx:** deploy chart 4.10.0 using hub eole image registry ([aa4bb32](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/aa4bb32b412854ea5519bce8b49b382f24e498b3))
* **ladigitale): feat(ladigitale:** add digiview application ([5c9f043](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5c9f0431450d5e70700e28270294857e9c101f12))
* **ladigitale:** add digiflashcards application ([3c03b6d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3c03b6d9c51f7a78c6a8833254fbcade2d2aae73))
* **ladigitale:** add digimindmap application ([d782db8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d782db891f8f06ab04bdf6b3f285e19a9cfb6740))
* **ladigitale:** add digiquiz application ([555baf7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/555baf7b6cd3c897bf6c3269e3389c1b80c01ff7))
* **ladigitale:** add digisteps application ([bf898d2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bf898d2c6197fbe5dfa1bca92a92a4a02f413e98))
* **ladigitale:** add digistrip application ([2b94842](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2b9484246e04c756e0fe6ec6d67172c50eebefc0))
* **ladigitale:** add digiwords application ([d0b8be4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d0b8be41daf32172b506b8d2526000a1ef009952))
* **mariadb:** deploy chart 18.0.1 from eole image registry ([428ba74](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/428ba749e07a65deeddf00cbd36d3f1d004b01f5))
* **matomo:** upgrade matomo version with oidc included ([c4abf5b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c4abf5bf890f68a2167133d7f3b388181364c9ee))
* **matomo:** use socle/external mariadb database ([d2a8572](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d2a8572eef9aa7f3f754f613197f2bda8a0847bf))
* **minio:** deploy chart 5.1.0 using hub eole image registry ([a186cd7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a186cd7140b1d3329eff50c4b4e1a09b7e578015))
* **mongodb:** deploy chart 13.18.5 using hub eole image registry ([5d6d9a4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5d6d9a4ea01410519b688c5e189497f76428f98b))
* **mongodb:** don't upgrade image to a major version ([ffcb835](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ffcb8351df4998f625c8ac2b77873fc4244d5757))
* **postgresql:** deploy chart 15.2.4 from eole image registry ([558a3e0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/558a3e0126e17ca017a8c803ec286860c8782e9d))
* **redis:** redis password is a new parameter ([3caedc8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3caedc87cccf93798f0eff2f8a5021f69afa9b6e))
* **shlink:** add first version of shlink ([46200da](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/46200dadd41b690901ff07e7c45a5481c0d2ee3e))
* **socle:** deploy laboite helm 1.10.0 ([f81eb37](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f81eb37e2d3b8c9eca3fe41ed639cdcee28e9428))
* **socle:** don't upgrade postgresql to major version ([c863a51](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c863a513a32a079b81991c566822dde4c5107ce1))
* **socle:** extend semver check for major upgrade ([bf98310](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bf98310778e0d4459226e979400167e289d434ff))
* **socle:** move keycloak client config to component section ([8ebe529](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8ebe52915d9834d002ccc9f70d3c21df4f88f06d))
* **synapse:** add s3 support ([ba6b7ad](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ba6b7adc7a60e74af4206194ed4ac3a7dc32a591))
* **synapse:** deploy chart 1.2.0 with eole app version ([d81758f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d81758fb76f1b594a85fed30f966d3780a34d1d8))
* **synapse:** deploy version 1.1.0 with e2ee off ([082ec69](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/082ec69de3ddd055327a8998743d4ee066d89e44))
* **synapse:** helm 1.0.0 and public room federation ([bfa74f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bfa74f4e4721b7acd3d995d4e0757fe76b752b3b))
* **synapse:** upgrade helm chart to 1.0.0-dev.6 ([5fc6419](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5fc641959a7324e30c891124b4cb072f3bcc82bd))


### Bug Fixes

* **addon:** correct db-script and initdb-job for mariadb ([6c4047f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6c4047f699e1bd90296517341efc0682641fd2c9))
* **addon:** list synapse addon in README ([426c614](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/426c614c3cb088a1fe05f0d65ede3c07d294629e))
* **ci:** add element ci test ([00b90ed](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/00b90ede8278f39a72b863acdc2aae5bc0954cb3))
* **ci:** add mariadb to ci ([3ccfb79](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3ccfb7927dec9731b259107b0a2f94d8aa77244d))
* **ci:** add redis to ci ([0ae4fdc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0ae4fdcfc773ca035497c8af117ed273d5477d10))
* **ci:** add synapse ci test ([de45e72](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/de45e72b02b960ac90c2af4a5d4ebe6601479946))
* **ci:** helm test always run ([3dd824a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3dd824ad4f3abe4946f8dbaadc8981d063f184ca))
* deploy helm chart version 1.2.1 ([fc952cd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fc952cd28ffe327c00ea0c2991ffa71b3c87d66d))
* **element:** deploy chart 1.1.1 with eole image registry ([29fa480](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/29fa480e7eafa1867aeffa803c53581144d0ce5b))
* **eole3:** -c option is possible at the end of the command ([1abe7b3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1abe7b3515c2d1c7cdc8823b93471bfa39dd389e))
* **eole3:** add infra and ladigitale types in mcm command ([6abbd82](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6abbd825b8a8bb989219deff566221c457fe2aae))
* **eole3:** add infra type in update command ([dc7f8e7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dc7f8e772a823b626399ba8d1ada1224d1491c87))
* **eole3:** take config at the end for build infra ([4cb80fd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4cb80fd5c4946c16d1f267750b10adddd580f2ba))
* **eole3:** wait 5s after if keycloak is in standalone configuration ([e14902c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e14902cf9983dc8973b0532e0d46efd34396e920))
* **infra:** deploy cert-manager helm version 1.14.2 ([5fabaa8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5fabaa8c76dd95c4e3ea7f1ad1d3e07d14513597))
* **ingress:** deploy chart version 4.9.1 ([5af59e7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5af59e72d32c6c45e2a43fe808611705a3eab634))
* **laboite:** enabling/disabling blog meteor parameters has been moved ([43a18b1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/43a18b1ee243a7777551505b2cecaa403e27d6ce))
* **laboite:** use correct ingress className for socle ([da3101d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/da3101d87997166a63790d372bac11cf2358c77b))
* **ladigitale:** generate accurate readme file for ladigitale ([6cde54e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6cde54e2705bb925cfdf3a2f0d96400b08998853))
* **ladigitale:** tablename is a parameter now ([5aaa1f6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5aaa1f68499d1092a73ca5fa89a7c2944e4e4046))
* **minio:** deploy minio chart version 5.0.15 ([faa08ca](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/faa08ca86178f73f11bbb730be0c4576e4aa03cc))
* **socle:** admapi user always add emailVerified set to true ([dd9ee93](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dd9ee935306621a1342c294daddfd57a95fe448a))
* **synapse:** configure s3 in values.yaml ([0176749](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/01767499d8119d9b4032dfe32daf69ecbaf6cb78))
* **synapse:** create namespace not too late ([fcce48f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fcce48f1b511f53e1588b14b19a0ea3c18c01977))
* **synapse:** deploy eole3 image tag ([b9a9304](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b9a93041cc209fe484c07ec57d4799aa80749a56))
* **synapse:** deploy image from eole registry ([9ca4391](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9ca439129531bb284db82082fd01e10ca6f955f0))
* **synapse:** deploy s3 resources in the good namespace ([3ad511a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3ad511a1ea395cfe8c3eba8e02b31df2279706ff))
* **synapse:** idpName is customised now ([5dcbffd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5dcbffde1fcd9de67c50e3487ebdde4702229667))
* use helm 1.0.0-dev.2 from hub.eole.education ([d630bc0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d630bc0d74a2dbabd8997699511c85815bac2dab))

### [6.1.4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/6.1.3...release/6.1.4) (2024-03-22)


### Bug Fixes

* **laboite:** add white domain to meteor settings ([2caa1ae](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2caa1aec2736a64d2c1b84fc1130b8135cada03f))

### [6.1.3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/6.1.2...release/6.1.3) (2024-03-11)


### Bug Fixes

* **keycloak:** deploy keycloak 22.0.5-eole3.9 ([22dc6c7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/22dc6c73602b3ce112eeb75556704fc9cfbe05a8))

### [6.1.2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/6.1.1...release/6.1.2) (2024-03-11)


### Bug Fixes

* **socle:** deploy laboite 5.8.2 ([d95d69a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d95d69a6efff6d63eb2232676672cc766c2179a6))

### [6.1.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/6.1.0...release/6.1.1) (2024-02-21)


### Bug Fixes

* **socle:** laboite socle subcharts were always deployed ([3702e79](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3702e79a5db33e63d63e2dcebabc508edfbe66ba))

## [6.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/6.0.0...release/6.1.0) (2024-02-21)


### Features

* **backup:** factorised backup and restore ([6ffe2ed](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6ffe2ed4a26236cc0d47cc06a206ee7820a92dd7))
* **database:** use single credentials file ([10532ff](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/10532ff327ce1b543ab276f5ac7d384196bfaf78))
* **discourse:** add postgresql backup and restore ([0f1b278](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0f1b278872baba1187416bb3d35e58fabe74d513))
* **hedgedoc:** use backup and restore macros ([7a98a11](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7a98a11b406dcb172a8da36baecbc7fec5af403b))
* **infra:** core dns patch can be done individually ([20b1a69](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/20b1a6940274478ae40092eb061c04b7ea874611))
* **loki:** add s3 backup/restore ([a5827fa](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a5827fa43f6b80d8011dce0f583775a5e24b356a))
* **mastodon:** add s3 and postgresql backup/restore ([e897b12](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e897b12ff2fd184751985b3bc241b3997d2ed863))
* **nextcloud:** add s3 and postgresql backup/restore ([5d235c7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5d235c737f23254956814956aba99486636ef847))
* **socle:** use backup and restore macros ([f06a860](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f06a86006b38cf8216a898a21bc040b3dbff1905))
* **wikijs:** add postgresql backup/restore ([8cd620a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8cd620a7ffcdc9e9ed89f06e55aa8d10f0956e97))


### Bug Fixes

* **ci:** add hedgedoc ci test ([b96006d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b96006d2b0fd6a303c96b831e3a3ae73045e3f96))
* **doc:** document override configuration ([1d5abf2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1d5abf2d37608b8e8d3eaf77dfdaf92e8d20e3fb))
* **hedgedoc:** ingress redirect with note hash ok ([e4c8bf2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e4c8bf2467393b8f719c23837cb8d8b6f16b694d))
* **keycloak:** deactivate some resources when hpa is disabled ([5bf660f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5bf660f292fe65b0d335abc0f5030991137ebf90))
* **loki:** create s3 buckets with a loop ([8c394d9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8c394d98237c7f5c57c8221cbd0f2d6f13d004f7))
* **s3:** backup and restore the entire bucket list ([babd17a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/babd17a128d422f43b3795402909a438f51dbfa0))
* **socle:** deploy laboite chart 1.9.2 for appversion 5.8.1 ([502b712](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/502b712ec9aa3e84aaeea8e4ed7b0891b652b825))
* **socle:** use database and s3 backup values ([6434bd4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6434bd4e94b285e22a66bb5519ea7ffd80133a97))
* **socle:** use get_value for namespace ([6ba7f48](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6ba7f48ec93da252d9836a074cdc479021bd6890))
* **socle:** use renamed resource database and s3 files ([7731de6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7731de682aa77131dd03e0bf6447a64a89e3ab2d))
* use get_value for registry value ([c65590c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c65590cab57ebb1dd6a13bad08584ce120c2aa94))

## [6.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/5.0.0...release/6.0.0) (2024-02-05)


### ⚠ BREAKING CHANGES

* **s3:** 'minio.[buckets,access]' keys moved to '<component>.s3'
* **smtp:** top level 'smtp' key moved to '<component>.smtp'
* **database:** top level 'database' key moved to '<component>.database'
* **eole3:** your custom configuration files have to be converted to
yaml format

### Features

* **addon:** backup/restore hedgedoc database and s3 ([8cc0fca](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8cc0fca2d64bf7525ec6e8b722755c7e7efe6e17))
* **addon:** codimd reorganize yaml parameters ([c6aa37b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c6aa37b878bc6cc9b560da22676e0d9b9add63f7))
* **addon:** collabora reorganize yaml parameters ([b491074](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b4910744e84184436c12da7c2f5c7d87ccc5c664))
* **addon:** discourse reorganize yaml parameters ([fe9fb37](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fe9fb378afdaebf6e65bad3e0cb250927cbb7721))
* **addon:** drawio reorganize yaml parameters ([bbb6a4c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bbb6a4c1b345d700ead0ed9be35e5152c39c29b2))
* **addon:** excalidraw reorganize yaml parameters ([ed0410d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ed0410dc2b1dfefc5d1da76bdb0e6d0fac0e01b3))
* **addon:** filepizza reorganize yaml parameters ([3fe6c98](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3fe6c98bc7feb35ad50f389698611d1bf103b18d))
* **addon:** mastodon reorganize yaml parameters ([a9e385b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a9e385b990346647c63e7955fe1c697917151337))
* **addon:** matomo reorganize yaml parameters ([6c54cf8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6c54cf808ab3ae978b717643f23ae4e7e5932651))
* **addon:** mobilizon reorganize yaml parameters ([c42d3ce](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c42d3ce1c5aafb242207f5f3b934d08eda419f3b))
* **addon:** new addon hedgedoc ([f013122](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f01312278b6b4780963434b60b8211fabcf2bb81))
* **addon:** nextcloud reorganize yaml parameters ([bb155c9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bb155c94127722b66feed34239458397bc47886e))
* **addon:** screego reorganize yaml parameters ([7a4f105](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7a4f10571c9aecc38144963ffbc501d92dcf514c))
* **addon:** wikijs reorganize yaml parameters ([6d761ec](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6d761ec7e84abcff925f9b049b2ed293af03a79b))
* **admin-tool:** kubernetes-dashboard reorganize yaml parameters ([310d43d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/310d43dc7bfa1721e2a3b164f48a793566f9cdac))
* **admin-tool:** loki reorganize yaml parameters ([cc9c820](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cc9c820b027d6c22f46dd3420af5d51c6c7d3224))
* **admin-tool:** node-problem-detector reorganize yaml parameters ([680fef8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/680fef843de27a0c69d98b4efd87ab9c590c67ff))
* **admin-tool:** prometheus-stack reorganize yaml parameters ([5bd2978](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5bd2978dc1381e428b110ddd84149cb97db3338c))
* **admin-tool:** promtail reorganize yaml parameters ([3735257](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/37352572a3d659a519048f5889bf24b35f237b3b))
* **admin-tool:** supercrud reorganize yaml parameters ([7a8e46f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7a8e46fc168617c2edf831de13bb37b7be966d5f))
* change chart-version to 12.1.0 ([8ae15d8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8ae15d8b82e0844c31209a6efe2d740ddfba17c6))
* change chart-version to 5.41.4 ([4360f1a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4360f1a5edcc6b97b7a82b9c137d36cf7eeb7558))
* change ingress-nginx chart-version to 4.9.0 ([691d693](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/691d6933014d008535792d321d703105657a29ed))
* change matomo chart-version to 4.0.0 ([2c1b684](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2c1b6843c6419f51ec56cc972114d01594c2c039))
* change nextcloud chart-version to 4.5.10 ([05ea4c1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/05ea4c1f6f88fb827029772d63729b97b8b74219))
* change the geo-location database ([6a87525](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6a8752589cbb5cd10de0bab0f97a0219882d2a0c))
* **codimd:** parameters factorisation changed some default values ([089552f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/089552f4754ba164d96306b7d1df53d8b0ea5bc0))
* **database:** new per component provider configuration ([cd3ea13](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cd3ea1390bcacc67ddc412c94259f1f77b9e89e3))
* **drawio:** update helm chart to 1.1.0 ([cf39dcc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cf39dcc881a57b5f44ddb939cb419d3279d95b9c))
* **eole3:** config files are now in yaml format ([2d7e9fb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2d7e9fbff1471f52d622d17371915dfc2fd03bb8))
* **eole3:** reorganise yaml parameters ([261a2ef](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/261a2ef8b9d467523d99394ce4a8db22e6c85d6b))
* **eole3:** replace 'true' and 'false' strings by booleans ([d256c9b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d256c9bd3d68422d2d65ae0891a31e7eb55c47b6))
* **s3:** new per component configuration ([3c0f4cb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3c0f4cbcfb53792d3232781855ad3afa995e63d0))
* **smtp:** new per component configuration ([c73e279](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c73e2794ea23849c616b8843e65788c0eb70cab5))
* **socle:** deploy laboite 5.8.0 ([4342e98](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4342e981587b99e6738120d4fbb2e2f735f81d5b))
* **socle:** use only one default config file (vars.yaml) ([015e31f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/015e31fdfe5d95f2123e7dc341eed22335d8874b))
* update chart-version to 55.6.0 ([c8f4585](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c8f45855aae0b5855312902c0b00716b752337bb))


### Bug Fixes

* **discourse:** create keycloak client ([f73a580](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f73a58097c1ff0efc67acc65caf32c002d062e9d))
* **discourse:** rename some parameters to be more explicit ([7fde359](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7fde359740f03c954ad0400b182ba253695b5447))
* **doc:** update tls files destination directory ([75d82b3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/75d82b3d74c52d098a4f4eb13a315a231508ec40))
* **eole3:** check update scripts removed ([f03aead](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f03aead4b45982dd6d0d4c9128070cef9c1825c8))
* **eole3:** parameters factorisation changed some default values ([f6a8148](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f6a814865d831870298f6285e45c26bb150650fa))
* **eole3:** remove useless remaining script ([5c5ea60](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5c5ea60188a9bc92a279fabeae426618652cbf95))
* **eole3:** test required commands at the beginning ([33cc303](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/33cc3034831b074905812c0f8b93a0e82024fa69))
* fix client-url ([4f0ef56](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4f0ef560b0788d5d4ad632e0dbb4394283767e83))
* **infra:** remove useless comments ([42fe18c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/42fe18cab2378e3f04bd8c770ea009eec8e0662f))
* **keycloak:** set good directory path for backup/restore ([57f8493](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/57f8493c5beb8b2252dd59bb6b1e35df3a79045e))
* **laboite:** update old and wrong parameters ([feeef5b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/feeef5b5dcc07df88005e2f332c306b43ed9af64))
* **matomo:** forgot old parameter modification ([9cd187c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9cd187c6792011c3b5ba4c26bdbdbbfdccfe6f85))
* **minio:** manage resources for minio ([b2862e3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b2862e31958a0d56cb96894dd7e00acf42f67068))
* **mobilizon:** double annotations correction ([25faf04](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/25faf049f712895bc5acaa4b397e7fdc66ef0dcc))
* **prometheus-stack:** update old parameter ([acfdc63](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/acfdc63c89ed4dc545569411ba6282a09ca843be))
* **wikijs:** create keycloak client ([a1dd2e5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a1dd2e5effb0ada5d77feff4fc4d1a5b5abb1149))

## [5.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/4.2.1...release/5.0.0) (2023-12-11)


### ⚠ BREAKING CHANGES

* **addon:** addon is not supported anymore. Please switch to gitlab
* **addons:** community version doesn't allow more the 25 users now

### Features

* **addon:** remove gitea addon ([47f6893](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/47f68936e674cba9080000581465f0f81ad1ffe3))
* **addon:** remove unused latelier addon ([88d5f07](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/88d5f079772f953313b034eb5848bfbd68706a89))
* **addon:** remove unused latelier addon ([0caeaae](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0caeaae74239c1632c162c9800f55f9995755953))
* **addons:** remove rocketchat addon ([8209a35](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8209a35858cc7e0e66321576c6d7b490ffeea6be))
* **addon:** update helm discourse to version 12.0.7 ([ba38897](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ba38897f68c39a3ca8a651704ee24588888139c6))
* **addon:** update matomo helm version to 3.2.2 ([bfe5810](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bfe5810c69a0090c87d0d1c0db1675e5cdceb652))
* **ci:** add ci for infra services ([4fd9655](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4fd9655219124bd82270b9aacf60d0de1d6cf942))
* **drawio:** update helm stable version 1.0.0 ([94446d1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/94446d1c208fce66e8cff824e90fe28071adace5))
* **eole3:** add infra subcommand for build and deploy ([dc78092](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dc78092bc90ab5176e975be608cf34716d11e74b))
* **eole3:** cert-manager can be deployed separately ([22b7bff](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/22b7bff4f19c6ec597658c7c67e2f1dacd69dcb6))
* **eole3:** ingress-nginx can be deployed separately ([b6f5262](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b6f5262aaebda2a3d4ec6d56a1166294afcad015))
* **eole3:** keycloak can be deployed separately ([889b780](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/889b780ec14bec3279a75e434842143796190fbf))
* **eole3:** minio can be deployed separately ([145cf9a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/145cf9a3a16ffeeb90467d0d1a9f870fd927f24e))
* **eole3:** mongodb can be deployed separately ([caca0c2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/caca0c272455af9ea256e44c44b8d3dbadfc437e))
* **eole3:** postgresql can be deployed separately ([f64deae](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f64deaecdf93e6f79bbf375bbec26e39f6160c29))
* permit to deploy each service with an existing database ([16f2770](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/16f27702a9c33b61583c61a1e5fb00c70f2d5236))
* **screego:** update helm version to 1.0.0 ([3715c51](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3715c51364033f16edf31b390064527d7e3f7bbf))
* **socle:** add settings for questionnaire ([ee772c3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ee772c3e306e6c6f89954aedbbe9c4f5309002ef))
* **socle:** deploy laboite helm 1.8.2 ([f53f607](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f53f60723044db685bc33694d64185dd9c4884d9))
* **socle:** deploy laboite helm 1.8.2 appversion 5.7.2 ([a867bc5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a867bc570e3992374b3203dc56de67f7348c08f8))


### Bug Fixes

* **ci:** remove gitea and rocketchat from ci ([314e4d6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/314e4d6c2fda37f6cb82b22cf29598c2ae0b7430))
* **doc:** document build infra and deploy infra commands ([ad4cacc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ad4cacc34289b0cc4a65ab6d6e47810cbe2fa633))
* **doc:** explain how to add eole3 bash completion ([227fa48](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/227fa48b8e74cb5c2b20d6a707d699e7407aaef0))
* **doc:** remove rocketchat reference from readme file ([36553de](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/36553de390547749f206f3835da35bc85d0ffc8e))
* **doc:** simple command line for basic configuration ([b6812bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b6812bfd49471221e1e090708b5f187393f39b69))
* **doc:** split readme file in two files ([780bed3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/780bed371bd23d2c68b939bfcea2d0eeccbcbfc6))
* eole3 must have at least python3.10 ([ece3029](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ece30291e609f674400b825c9d30c11988a587f5))
* improve deploy output ([ed1182a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ed1182ac14b67f591714d8a3228eaecfa927e6b6))
* improve readme displayed in build addon and admin-tool command ([dd7ecc9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dd7ecc98f2f41e1225976f0b18ef4667527efffc))
* improve readme displayed in build socle and infra command ([3beb6f7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3beb6f7be6c35851539be12c41976511a8534cec))
* **socle:** deploy laboite 5.7.2 ([38be81d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/38be81ded607cb3d31d267fff5bc5aeb308e8467))
* **wikijs:** fix image tag from latest to 2.5.300 ([63e115b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/63e115ba09d0b04651783c094bf79e5344cb135d))

### [4.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/4.2.0...release/4.2.1) (2023-11-22)


### Bug Fixes

* **pyproject:** `readme` is too long for gitlab metadata checks ([8c76b6d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8c76b6dacf359ecb22c08e87b8230678235aff3b))

## [4.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/4.1.0...release/4.2.0) (2023-11-21)


### Features

* add undeploy script ([ce65a77](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ce65a771dfbbf1d9eddbd2810d1bdc2e9997b9e9))
* add undeploy script ([0d12aa8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0d12aa8d57627e03fd9da6938b2114d68bbe0fd9))
* change loki chart-version to 5.36.3 ([da35485](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/da354856d79d86cbf490154734d76a91cdc4a374))
* change node-problem-detector chart-version to 2.3.11 ([155656d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/155656d59821f967920a04c6b86e5bbbe3d70243))
* change promtail chart-version to 6.15.3 ([69e1ec0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/69e1ec0dca4c8062a1242d08e6e9e1751f318d7f))
* change promtail chart-version to 6.15.3 ([72e879d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/72e879d701227204124aa50f4b285f522c90e3ec))
* **eole3:** add mcm command to minimize configuation ([0e11702](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0e117025d84ddcb8557d10fe76991526384c63ac))
* **eole3:** add outputfile option for config command ([878af1d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/878af1d2c421ac6409f10141fdc58a29755d3b75))
* **infra:** enable real-ip for scaleway provider ([a2ff9d1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a2ff9d1bf1fd8884ef8676cb0ed5b37d9b22645f))
* **prometheus-stack:** grafana dashboard can be customised ([465ca5c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/465ca5c2e221d7423a3c45f77ae276f5cfe29206))
* **socle:** backup pvc size set to 20g by default ([00d9191](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/00d91910c05eca4215e8dfe0f41865761fcc955f))
* **socle:** whitelist ips for laboite and subcharts ingress ([a20ec32](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a20ec3286fb9df28d4444e320cfd1bcea3ea8473))
* use the crds subchart to delete all crds resources ([9fea77a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9fea77ad235434ccafe2affee985f5ba90f18230))
* use the crds subchart to delete all crds resources ([de89c12](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/de89c12755433e0abbc665db2c0a70cc0d21c0b3))


### Bug Fixes

* **codimd:** add mandatory openid and profile scope for oauth2 ([492f3fa](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/492f3fa1c834cd5d1333a38348481f376cb0df85))
* correct minio url in minio-credentials template ([3d8d71e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3d8d71ed7d38239799c524a4e0e3ff7acc7729f7))
* **doc:** update readme file ([9656128](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9656128f9372b17df0baaccc8b2714296fe48a46))
* **eole3:** add missing docstrings in help ([c940fd2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c940fd279368f4d71d7c1cfd90b24e1b86926a0a))
* **socle:** deploy laboite helm 1.8.1 for lookup-server without api_key ([10bf6eb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/10bf6ebe1ed86665a327c23d87bca1521fa90ad4))

## [4.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/4.0.0...release/4.1.0) (2023-11-08)


### Features

* **socle:** deploy laboite helm 1.8.0 by default ([0903e98](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0903e982c8ec4a37094475eca3151067fa45955e))

## [4.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/3.0.4...release/4.0.0) (2023-11-08)


### ⚠ BREAKING CHANGES

* **addons/admin-tools:** for build addon and admin-tool, --config custom-adddon/admin-tool ini file needs to be
after addon/admin-tool command

Config files are loaded in this order:
1. Default socle config files (`eole3/data/vars.ini` and `eole3/data/cluster-vars.ini`)
2. Custom socle config files (`--config` option before `addon` or `admin-tool` command)
3. Default addon/admin-tool config file (`eole3/dada/[addons|admin-tools]/<serviceName>/<serviceName>-vars.ini`)
4. Custom addon/admin-tool config files(`--config` option after `addon` or `admin-tool` command)
* **addon/nextcloud:** nextcloud database configuration is moved from
`nextcloud.db*` to `database.*`
* **addon/mastodon:** mastodon database configuration is moved from
`postgresql.*` to `database.*`
* **addon/wikijs:** wikijs database configuration is moved from
`wikijs.db*` to `database.*`
* **addon/gitea:** gitea database configuration is moved from
`gitea.db*` to `database.*`
* **addon/discourse:** discourse database configuration is moved from
`discourse.db*` to `database.*`
* **socle/keycloak:** keycloak database configuration is moved from
`keycloak.postgres*` to `database.*`

### Features

* add oidc script and adapt deploy and matomo-vars.ini ([515e3d1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/515e3d1a82126a70e527fee19e3752ab0e5988ef))
* add questionnaire cron default values ([358fec9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/358fec9f4927dfebc7c8c47edadce31cf76c6eb0))
* **addon/discourse:** new database configuration ([32c6aa9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/32c6aa93ed13ae1ba94808404eac2038ed56d70b))
* **addon/gitea:** new database configuration ([6c562e6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6c562e68580c69305aad1d3ffa306b0ffe8680f2))
* **addon/mastodon:** new database configuration ([56df131](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/56df131afde9fa2b699539786f6506db0d99fe33))
* **addon/nextcloud:** new database configuration ([66651c3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/66651c31ffddb56d3b6e317d336de2a75fbef918))
* **addon/wikijs:** new database configuration ([b3e7188](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b3e71889bc9fce29f1b05757562974b3b5cb6339))
* **addons/admin-tools:** add --config option for addons and admin-tools ([f9921b9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f9921b97d8a83d0fe48770e3fc1b565c775a2c92))
* change chart-version for discourse to 12.0.0 ([cb31809](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cb31809a31965ee5acf5bfaf65daa4bcbb10c55c))
* change chart-version for rocketchat to 6.4.1 ([d84e72d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d84e72dfd442043126941b88db55f87ae2f17f93))
* change chart-version to 3.5.14 ([8d37f73](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8d37f73e1c2c798c158ab90ed94f5efc422534af))
* change chart-version to 4.8.0 for ingress-nginx ([5923e42](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5923e42a929ed626b59cf01351107baff310601d))
* change prometheus-stack chart-version to 52.0.1 ([dd2c59f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dd2c59f6361b2ba3e77211d65475e853b2ebd437))
* change to chart-version 3.5.15 ([08bdd6f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/08bdd6ffc5d22a7e2e521807628c883bb31c648d))
* **database:** new helpers to configure PostgreSQL database ([f623943](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f623943b06302d285923cd1a90f441e68d9d4ae7))
* **eole3:** add update command ([439182e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/439182e0cf81a240c72ed6d08b0eb0059acd074d))
* **keycloak:** add default locale for the realm ([cc489ef](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cc489ef16287e92124db9be168e0bd395009c9df))
* **keycloak:** an update should initialize keycloak if required ([fa957bd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fa957bd6cefc3ef0844af03a52ea90a1eafcb8d7))
* **keycloak:** cleanup ingress before upgrade to keycloakx chart ([ac8e827](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ac8e827b12344d15740b103a00ad167f7b1e38a3))
* **keycloak:** migrate database to socle PostgreSQL ([cdd78a6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cdd78a6bb59131e40d6f88a15d6db67cb7a5979a))
* **keycloak:** update `values` template for keycloakx ([06e0b1e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/06e0b1e750a9b801d07e56de6b040396a80912c9))
* **keycloak:** update to 22.0.3-eole3.4 ([404cc44](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/404cc44c51fec17b00c58959f0af848c606d2a82))
* **matomo:** new service addon ([09b0258](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/09b0258eb66896afa54136d415230a2bf7ae72f3))
* new env var for lookup-server 1.0.0 ([550e123](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/550e123f775fefb43bae7c73d9c0d2cacad046a3))
* **nextcloud:** upgrade chart-version to 4.3.1 ([4ae6e72](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4ae6e728a84dae3e12367b8d600e3a2e5f21af04))
* **socle/keycloak:** use new database configuration ([35b4b51](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/35b4b51a20282b4332141c287e2e19594b276d95))
* **socle:** add var hidegroupplugins ([17d6ebc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/17d6ebcfe4c89f4d779047f320577a046de4ab31))
* **socle:** new non-production PostgreSQL service ([01abca9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/01abca98683ab42934bc88eb6052fca9429f47ad))
* test new chart-version 5.0.14 for minio ([4718158](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/471815818333ff600393b14cd56c15dc579c2b35))
* upgrade gitea chart-version to 9.5.0 ([9b638dd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9b638ddef22fd66c07b7c3a0ad26d14ed609c409))


### Bug Fixes

* activate demo mode in redis-cluster and postgresql ([0c04c6b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0c04c6b5257c056918db467991ad03346356f330))
* **addon/minio:** don't use hard coded namespace ([7c2f0cb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7c2f0cbec1cb5837c44d86250df2e5667d12442c))
* **addons:** add correct namespace to mongodb uri ([5df63b9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5df63b97d14c70a7f5b6554bda3cb33a16ecbf58))
* **addons:** always download clients_tools image ([661d962](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/661d962c350f9ec8738036f1f5aa3eca4d1d9f34))
* **addons:** use default chart tags for minio server and client ([772f2b2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/772f2b2c2b285f32de2c66f06a45bdd0144c8e00))
* **admin-tools:** add correct namespace to mongodb uri ([ff7cf4d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ff7cf4df5731e1876d2362547c897d8fa59e6ad5))
* **admin-tools:** always download clients_tools image ([123973f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/123973fcc7db5b08d5c69d3f232ee6b9e4726177))
* **admin-tools:** use default chart tags for minio server and client ([ca24915](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ca2491530d8f1b39284a6cbbe17c033db2010dc3))
* **backup:** keep the lastests backup jobs ([3e55019](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3e55019f706f75d05c158971d84467d4617fad6b))
* **CI:** don't replace __version__ in cli command ([c56a5e9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c56a5e984af2753c64097e55f696dceeef482d25))
* **CI:** restore --version command after CI issue ([3ac1242](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3ac124296c87e69fac55c4c1094a90b4b67b0c1e))
* **CI:** restore --version command after CI issue ([b44df52](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b44df52ae21e1f750509fbc4d3938adba91ea401))
* **collabora:** disable autoscaling by default ([df1a597](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/df1a597df333403b33527eaad6d27ca8bfd57b51))
* config options before build command are used now ([ab6fe08](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ab6fe0846acaa490971a796a56e7454f69aaed8c))
* configure values for resources section ([dc268f5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dc268f526ea3af96eabf971f16a2d1e3a984db45))
* create namespace before database resources creation ([ce25f01](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ce25f01af099f8321f469ae5a3fd3a11923b6b6a))
* deploy supercrud chart 1.0.0 ([7e45f90](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7e45f90f3c7f7a15cdd1b0493b4997cdf2a9277b))
* **deploy:** deploy script can now be executed ([c5bb914](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c5bb9148de2eae93915f84c32f0d26c9b4363c16))
* **eole3:** update jinja2 dependency to 2.11.0 and superior ([9a8f118](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9a8f11860a3c97f7ae11f7061b44b0f945ea1fa7))
* **gitea:** no persistence in demo mode ([15cf89a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/15cf89af4633ec3f5113a38195e842f896e25067))
* **gitea:** oidc is available now ([e74e88d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e74e88dbb1c39d2974a1bf2b6368bc7038819a04))
* **keycloak:** add PostgreSQL credential before restoring ([584e975](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/584e975f2f53befa2d19c6eb88542c9033ca6e8c))
* **keycloak:** backup restore privileges are not usefull ([d54571b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d54571bd195c32469f970e666f3a7ddb45fcc9fc))
* **keycloak:** wait for keycloak with rollout status ([18f9633](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/18f963348c9454c42c66fbb65d5f2ad3f74ecb97))
* **keycloak:** wait for service before updating the configuration ([3c18284](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3c182842959d9f7e70963bcb281f155bbc70c8b1))
* **mastodon:** correct smtp settings in deploy script ([c64f2ed](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c64f2edc3e45f9ef588b8f96e495e56780cc0dc8))
* **nextcloud:** persistence can be enabled ([a7cdcfa](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a7cdcfa91b9f1af7da8447dc38d13fb8b457afdc))
* **prometheus-stack:** upgrade crds and release work now ([64368ab](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/64368ab44c6f5aeb99424533f696501da770fd58))
* **readme:** remove deprecated admin-tools ([6d55d12](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6d55d1240c12999cb5eac8371876022b856c12cd))
* **rocketchat:** remove unused initial_user env var ([4c56cd2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4c56cd28a1cf0571ffd9a1fd0c46c1301bea0fe9))
* **socle:** add correct namespace to mongodb uri ([35d7993](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/35d79934a6435dd300c8f95116dcaf672f5640c1))
* **socle:** always download clients_tools image ([552f433](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/552f4337162362c1b97924032b2e345fe1f81187))
* **socle:** deploy laboite chart 1.6.2 by default ([4688163](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4688163c73f5b7f65089e1fc3862e19623a4d8f3))
* **socle:** minio must be ready before create bucket ([f17ef98](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f17ef9891aa761098e6ff5d43099d8d99e7b38c0))
* **socle:** minio script always download clients_tools image ([3fd71a9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3fd71a9c9f8a1bb2ab43fd6707ecb0e81379669f))
* **socle:** postgresql jobs always download clients_tools image ([7f9edb9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7f9edb9266f412861dc58e3e38c8028636a0c68f))
* **socle:** restore can scale up and down keycloak ([313d423](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/313d42320e15e431f01db83deed9c3d81d189966))
* **socle:** use default chart tags for minio server and client ([ee5c631](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ee5c63187a4c58199ca7856a637a4dd1e8fd2075))
* **supercrud:** deploy chart version 1.0.1 ([04c5a06](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/04c5a06f8002ccf1ee11e5fea76c305f4605c452))
* update geodatabase and refactor deploy script ([7a62aed](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7a62aedd70e63924541462452cf71304c2fb00da))
* update readme ([82501f7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/82501f76bc6c22f47f6536b12ed1706a6f80b722))


### Reverts

* Revert "feat: add circles settings" ([7ef4225](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7ef422555f5644b4d56ae9ecb572703d6131b1ed))
* Revert "Revert "feat: add --version option"" ([f823134](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f8231348daf8b9d06fcc0e0756da7288509441e9))


### Code Refactoring

* refactor matomo-values.yaml ([ae72801](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ae7280124e2cfda760e5d9b5baa85ab7053b554e))

### [3.0.4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/3.0.3...release/3.0.4) (2023-09-05)


### Bug Fixes

* **laboite/smtp:** fallback port depends of TLS activation ([845ff25](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/845ff25e2943508947167ae27766cac1f161a979))
* set disabledfeatures-bookmarksfromclipboard to true ([8ccee6b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8ccee6bbe6591d979e4f12d020490b14190cbce2))

### [3.0.3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/3.0.2...release/3.0.3) (2023-09-04)


### Bug Fixes

* **ingress:** templatise replicacount for ingress controller ([7673830](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/76738307bd588bb46e9bdef87c677bd29cd24292))

### [3.0.2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/3.0.1...release/3.0.2) (2023-09-04)


### Bug Fixes

* **portail:** deploy laboite helm chart version 1.6.1 ([2b05d71](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2b05d714435b1bcbbee6e216041d88a8a98bd6c5))

### [3.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/3.0.0...release/3.0.1) (2023-09-04)


### Bug Fixes

* hide checkbox for nextcloud group share ([7f0b623](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7f0b623bdc3b4ed7cef010885a3fc15443609318))

## [3.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/2.0.3...release/3.0.0) (2023-08-31)


### ⚠ BREAKING CHANGES

* **mastodon:** move smtp values from `[mastodon]` section to `[smtp]`
section
* **discourse:** move smtp values from `[discourse]` section to `[smtp]`
section
* **discourse:** set `tls` in `[smtp]` section to `true` or `false` to replace the old
`smtpProtocol` variable in `[discource]` section
* **socle:** smtp port is mandatory in `[smtp]` section. If the smtp port is specified in
the `hostname`, it must be removed

### Features

* add analytics settings and disabled by default ([a28fe08](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a28fe0870c5385e41fd8f85160020df8f98e9e03))
* add bookmarksFromClipboard to disabledFeatures ([10134df](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/10134df0aaeb62dbe7b7ad4de7bf658414eb5ab2))
* add circles settings ([3fa7e84](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3fa7e8420a1a1a0518272eaad0ec0393c10c7853))
* add feedbacklink ([bf36107](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bf36107cf2003a533762be196feb8b2a35c0b7fe))
* add francetransfert settings ([fe6b723](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fe6b723bf9511f8936015d8b054dd4c1aa892bb7))
* add onboarding settings ([2326e32](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2326e323a977b71ba14ae5cb86bd30f77d493735))
* change chart-version to 10.3.4 ([83ca851](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/83ca851e9ecf8f525b1e5ac98fce5b1bf1f1eca0))
* change chart-version to 2.3.5 ([1ac887d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1ac887d2295b86197e0545aea6ad9c9b44ad8bc4))
* change chart-version to 4.7.0 for ingress-nginx ([44ab5fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/44ab5fc6c2f4d2005cf8a9c1e2d1016902fc9ed6))
* change chart-version to 4.7.1 for ingress-nginx ([ec466ab](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ec466ab795ff14bcf8d4d96586b56854d9c172e9))
* change chart-version to 5.0.11 ([36255ef](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/36255ef3a91e0cb7a7fe7e0b486cab85dade6ea5))
* change chart-version to 5.8.9 ([66db5d5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/66db5d5d4729f4f7ca6a5bbb2cac7ab3d10068e0))
* change chart-version to 6.0.8 ([16fbe6f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/16fbe6fdce1f2cabf77e46a0325c88ed74321976))
* change chart-version to 6.11.5 ([3b424f2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3b424f2480fb7eead7cbcaa70bf5530f6530bcc8))
* change chart-version to 6.14.1 ([8b8814f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8b8814f3ffdf4dabdce72b07cc5b903e91ef0ce2))
* change chart-version to 6.2.4 and disable the mongodb dependency ([f60d3fe](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f60d3fe3fb0eef7b8572aa18863a55006d8caac3))
* change prometheus-stack chart-version to 47.3.0 ([f9b9f94](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f9b9f9457e808a881756121a7decfc71ef823f6b))
* **command:** add deploy command ([3c4bf17](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3c4bf17ae2468bd981985c1f09c06e8c3c40a97d))
* **discourse:** smtp parameters are in `[smtp]` section ([c1c2ef0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c1c2ef000b663dfa1953c1b77034a83b27707b1a))
* integrate laboite-api into laboite socle ([ee5eb29](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ee5eb2943734f6bec3d642d5b282d26fd1febdda))
* **mastodon:** smtp parameters are in `[smtp]` section ([fd1dabc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fd1dabc3452d55971d921500302d77c114379f4c))
* new testing version ([e9b0059](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e9b0059c5caeb16a3efcb4ca508c906fded0a4df))
* publish new helm version 1.6.0 for laboite ([95dd92d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/95dd92d4bcc8b41015b88bbedc227ed7406a8027))
* publish new stable version ([0378f7a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0378f7a290cf749e36e54794024af31930121747))
* **socle:** use smtp port to build meteor settings smtp parameters ([c8c6ee5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c8c6ee505afb5435343b941d4006632bad68c540))
* update mongodb chart version to 13.16.4 ([976bb4e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/976bb4ec6a80db669255c05ce410b7789975d2f4))
* update prometheus-stack to 48.3.2 ([a0f8d14](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a0f8d142ff5533b9a7a407b91ca6000fbe215ae7))
* update promtail to 6.14.1 ([21659a6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/21659a63330cf72c5ee1a62d6a290d7c21bc83f8))
* upgrade minio chart to 5.0.13 ([cdae07c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cdae07cf390755a8e747ccc26aee5fb127a175d3))
* upgrade new chart-version 6.3.1 ([bf0018b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bf0018b717d17991731076d8e6d117b1c5e84ecc))
* upgrade promtail to 6.14.1 ([a70e7e6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a70e7e6256e45c0cb66daf1ea02dc89956342788))


### Bug Fixes

* add meteor-settings paramater for questionnaire ([988b70f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/988b70fc99eafefaceada05104470c17e78a6aa1))
* add missing bracket ([ec969d1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ec969d144796230dd552ed444ef5a248698ef4ef))
* add vars in meteor-settings.json ([41e749d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/41e749d587d88b19239c90e480adfafb92c8600f))
* **codimd:** update minio client command for the download policy ([f362203](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f36220395e0ba61133939993fd91e32fdbcf4236))
* **docs/changelog:** use correct gitlab URLs ([d574a2a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d574a2a9d44f09761419f4e4191ef60f79747fce))
* loki singlebinary replicas should not be one ([7553ad6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7553ad6f2f8e7cbff28b9592372d564adb53684d))
* questionnaireurl setting with upper cases ([22e3691](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/22e3691edef8cc58843a99b77ce7c56ccd44f8d3))
* remove garbage from last push ([5c122cd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5c122cd00847876eef322a13b4462a1be532eda7))
* **semantic-release:** default generated URLs don't work with gitlab ([504e6f0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/504e6f0af7eab55c4a006a9ec0efa45e3828f4da))
* update meteor_settings.json with new groupurl var ([fa1180a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fa1180a7736635d72a3fa51f87a731c770ab31e3))


### Styles

* eole is now the default theme for laboite ([832ca09](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/832ca09084c9bb6f57fd0ff726e8f7edfa414d94))
* theme default value for laboite is eole ([2a25aed](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2a25aed7692387d536f25cf2043ea83793c0d723))

### [2.0.3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/2.0.2...release/2.0.3) (2023-06-15)


### Bug Fixes

* redirect to index to view restricted note if not logged in ([4df7945](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4df794549c5f98bfefc8d9266260c5e65c926d2d))

### [2.0.2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/2.0.1...release/2.0.2) (2023-06-14)


### Bug Fixes

* cspframeancestors elements are between double quotes ([3ba2049](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3ba20493acde58cf58ded3db29201db5e1ee7951))

### [2.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/2.0.0...release/2.0.1) (2023-06-13)


### Bug Fixes

* add meteor settings parameter for the pastille ([8fe3399](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8fe3399f91b32233b6ffeabacc55be2e9598cdad))

## [2.0.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/1.2.0...release/2.0.0) (2023-05-30)


### ⚠ BREAKING CHANGES

* **config:** `--configfile`, `--clusterconfigfile`,
  `--addon-config` and `--admin-tool-config` options are replaced with multivalued `--config`
* **packaging:** the commands are now `eole3 build socle`, `eole3 build addon` and `eole3 build admin-tool`
* **packaging:** the configuration templates are located under `src/eole3/data/`

### Features

* **config:** configuration files are define with multiple `--config` ([fdba3d5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fdba3d57737c201a8bc4ae5e8f66812b34ed6552))
* **config:** display configuration merged from default and options ([e905f23](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e905f231fcf004861a1fa0c15f0dcf2ef24e4a39))
* **options:** share configuration option ([87f7855](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/87f7855d28b27e072116cf7611e73a0ef3b7bd81))
* **packaging:** import Lolo work ([1cc060a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1cc060ab89ff101bac5be508b3eb10fc0e54b615))
* **packaging:** use pyproject.toml and hatchling ([ad158c4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ad158c4c3cbe13de45b1f5e4ecee72717539e70e))
* **release:** publish python package ([43152be](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/43152be1d06b2948ab2f30933aad8e93a6b51ebd))
* **release:** update `semantic-release` configuration ([74106db](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/74106db0ee3d541ad8ef60ff52867c3b3420027b))


### Bug Fixes

* add correct helm test file for mobilizon and mastodon ([85bcbaa](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/85bcbaac4ed16a120f216882647510a34010aece))
* add defaults values.yaml for cert-manager ([4fad06c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4fad06cdec5ad58e631d0ca84e9dee07963ad9da))
* add missing addons and admin-tool to ci ([f463a83](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f463a83769290a1c0d5f944390b141270355c180))
* add wip in releaserc.js ([bff6c38](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bff6c38dd0a04bbfe64a431cf531494fd63408c9))
* change namespace from laboite to cert-manager for cert-manager ([f0a3f09](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f0a3f096a5239232d15fc6667565455a796422c1))
* delete init-minio job when success ([f6e6f25](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f6e6f2572b01975d75f22e7f476953abbeb2e430))
* install CRDs for cert-manager ([fa3e17f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fa3e17f1af206a569d3d96d6989019183820a84d))
* minio bucket and user correctly initialised ([f089431](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f08943116dd48c5ada7b701458a3c145361afbf2))
* minio helm chart is in version 5.0.9 ([9bc2782](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9bc27825cda993f15568f599f3803b24e1e1468e))
* minio job doesn't run if minio desactivated ([151521e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/151521ec6c89d12143f82bb07279bc93e1d7d8cd))
* typo in deploy script ([104ad35](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/104ad350253bc4688093d926d93f383ca2822df2))
* update chart version to 2.2.20 ([1ed9e27](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1ed9e2734e7264c09770e22dfc4a163924114ef4))
* update excalidraw helm version to 0.5.0 ([2daf8e4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2daf8e4f75b92a4906b1a76be0c36d0b42195130))
* update helm chart version for kubernetes-dashboard to 6.0.7 ([03b6696](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/03b66968a7490754da09212b4d2492157bedc5a5))
* update helm version to 3.5.4 ([4b1631c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4b1631c5537ae5e5d523752014d340ebad82b402))
* update image filename for geolite database ([f028f6a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f028f6ac437c4e53d3b425a6fca8a10c4f79c93c))
* update ingress-nginx new chat version ([73f3e2a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/73f3e2a43a8f251c0b7d41e44b04426a5a358d4c))
* update minio server image tag ([33d30dd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/33d30dd8cd8fb4491d5be4dc12b72df99aac0a5e))
* update mongodb helm version 13.9.4 ([3de188d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3de188d617ea515400d5e2d9d5f80dad684f4247))
* update promtail chart versio to 6.11.2 ([84cf23b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/84cf23b5b876c73e8c6c6295e3e941b6f9726390))
* update rocketchat helm version to 6.1.5 ([13f2747](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/13f2747207cb230e542f52fb8597858c962fc2af))
* update to chart version 2.3.4 for node-problem-detector ([e1edafa](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e1edafa02d51f265bb45adf81c3e00ecbebfa446))
* update to chart version 45.28.0 for prometheus-stack ([38ed678](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/38ed678504c297adebf1c1060143bd22210e1f0e))
* update to chart version 5.5.1 for loki ([9337778](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/933777833ac46566b4e0683d14cb4896f665b3d0))
* upgrade gitea helm to version 8.3.0 ([c86019c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c86019c649a2271a1f23439eda483f8ecf38f3ec))


### Documentation

* **readme:** update usage for new commands and options ([91b8a8a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/91b8a8ac7234f392fdd374ccbda50dec49039be7))


### Continuous Integration

* **build:** build and install the python module in venv ([092caea](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/092caeac0c6caa7a6018f27d9087458bf90add40))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/1.1.0...release/1.2.0) (2023-05-09)


### Features

* add new var in meteor-settings.j2 for api user creation ([717ebe8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/717ebe8bea21b0306abf2fcbc08ab767aa4ad8a0))
* publish new stable version ([d2c6dd1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d2c6dd1df3093a6a86a9d19c20d8d7fadfdc201a))
* publish new testing version ([18db9bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/18db9bfe83c96bf9a5c50af28c71da4f9767a2ff))
* upgrade prometheus-stack chart version to 45.3.0 ([82c4688](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/82c4688f1593bea4fb994d3a1b3b007d73435344))


### Bug Fixes

* add enabled=false if using external database ([4037ea7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4037ea719bbc15f59ee6c57282dbd3faeef6b630))
* clean mastodon-values.yaml ([941ff44](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/941ff44a5c4d077a5a15fd59d045d628c696ac70))
* **codimd:** increase memory request and limit ([fea5bb2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fea5bb2414637b0c362d1148a7347b056d2d163b))
* demo mode is not enable by default ([e52489c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e52489cbff0b59713142f95cf85a8bb9706b4bce))
* **mastodon:** refactor mastodon deployment ([0510fe6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0510fe6b1aa827dd950a3d72e319e7ece1d05803))
* restore enableKeycloak paramaters in meteor-settings.json ([72480d9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/72480d9ce085f26614aadad10143dd5d22de5391))
* **socle:** wait ingress-controller to be up ([7e03703](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7e0370300e1f3b47fcecdaea41105f7c00363e57))
* upgrade minio server and client image tag ([025d50e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/025d50e6ee4e3bcbb76ec51dd87f54d4e70ae514))
* use correct chart version for laboite helm ([1e14df9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1e14df98e872da925a1d0708198aa6fca4d11e5b))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/1.0.2...release/1.1.0) (2023-04-18)


### Features

* a new ingress domain_name redirects to portail ([e152023](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e152023d12fce32c7d11b244107ab1f60ebd116d))
* add check_update script ([823a68f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/823a68f80e66f9aa27dab9b0e260b8ed2e553278))
* add check_update script ([2e9ba92](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2e9ba92fc6359119d4e6e7fe3234761b96194c04))
* add collabora addon ([71fbf57](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/71fbf5749af74731121070d7a2537d067308ba64))
* add nextcloud collabora support ([706d9bb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/706d9bb45af026fcb24528d4162bcea6ec4e426b))
* add questionnaire as laboite subchart ([afe740c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/afe740c7b42758600e8afc66b27d99355a4ea513))
* **addon:** add mobilizon to README ([c767399](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c767399ad350acfbf42036a649047e765e412756))
* **addons:** add mastodon ([76d58e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/76d58e187fdd3c9bea6d3a3fda4bdc7d4602d159))
* **addon:** use eole harbor registry for codimd ([1c4db77](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1c4db7798791a9a42ce571932dc7fa5091cb00aa))
* **admin-tool:** chart version updated to 6.9.0 and appVersion to 2.7.3 ([7902dde](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7902ddefb8b2d582557b752d43af481393540243))
* **admin-tool:** configure persistence for prometheus-stack ([cb0182f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cb0182fcc0d94f70d5ea739f66ba016a73a1a44f))
* **admin-tool:** loki set grafana datasource via api ([32940cb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/32940cbc36ffc91e52dae85c38a3fd29132aafaa))
* **admin-tool:** manage prometheus-stack crds ugrade ([07517bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/07517bf31f6336f79dc7ea596d96208c57f538b5))
* **admin-tools:** add grafana dashboard with api ([b2870ea](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b2870ea357ceb4fad76d79f4460baf3af4bf72f7))
* **admin-tool:** update prometheus chart to 45.2.0 ([1206763](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/12067639bee38596a688de098a10259015dfc307))
* cleaning values.yaml template and update chart version ([e08f868](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e08f8686db59d96fbd83f84769170d8f4e7bd422))
* **loki:** add possibility to configure external minio ([8414e6d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8414e6dff1c859fe317f324315a87927ae2156da))
* **loki:** upgrade helm to version 4.4.0 and loki to 2.7.0 ([20565ad](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/20565ad8c64fc573036f472a97decdfe66e42cbf))
* **mastodon:** activate OIDC with keycloak ([7bdc2d0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7bdc2d03e1a91117619bc5b5cc4bbfd76cbef6ba))
* **mastodon:** add mandatory secrets to vars.ini ([8307c00](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8307c00be309ad813196f885d229fde741627208))
* **mastodon:** add parameters to configure SMTP ([81487d3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/81487d36fd67e22a59bff820c96c9ad21addf9f9))
* **mastodon:** add password for postgreSQL and Redis ([46ed6fb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/46ed6fbce276fdaa9b778f2900ed72cd06d79bb3))
* **mastodon:** adding a script undeploy and a script to delete all data ([54a8295](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/54a8295a43e0f842b09acca8a5c60f004af03abb))
* **mastodon:** allow anonymous users to download objects only ([7fb2e75](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7fb2e75671f0a48bd378d399f56d04e07e66788c)), closes [/github.com/cybrespace/cybrespace-meta/blob/master/s3.md#setting-up-aws-and-an-s3](https://gitlab.mim-libre.fr/EOLE//github.com/cybrespace/cybrespace-meta/blob/master/s3.md/issues/setting-up-aws-and-an-s3)
* **mastodon:** don't deploy if a mandatory variable is missing ([39a1f3c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/39a1f3c9554bff43f7a5df0d4d4212ba5947c8d6))
* **mastodon:** handling of ingress nginx ([a239442](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a239442c4e076466cfb3ff31d7efe2eebbb48f7a))
* **mastodon:** locale in franch by default ([3124e55](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3124e5542877c8f0f79b2a9038ad9ba8f723c9ee))
* **mastodon:** remove keycloak's client if we want to delete all data ([b7f0c2f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b7f0c2fd6254ad5ad50082464f2f56fb40d2cf40))
* **mastodon:** use a different s3 bucket than "apps" ([70bf55c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/70bf55c5772d5e9b0da5aacfe51bf50bf0ff39de))
* **mastodon:** use a different user for minio ([5516c91](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5516c91901e75ef8952a0ec816e2b42185ce6e51))
* **mastodon:** use minio ([e800b42](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e800b42228e49f2a1b5be57ef3da4c5843ce8268))
* **mobilizon:** add service mobilizon ([163b75a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/163b75abc6c20cbe149faf6a7b98d9797b6e62f2))
* **node-problem-detector:** add node-problem-detector admin tool ([a0602d1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a0602d1cba2ed208c7b60068f4284986eb3a08da))
* **NPD:** enable metrics and rules for prometheus ([fde2059](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fde2059ea63e65b2568177b26c8022492ff2b82f))
* **prometheus-stack:** accept any PrometheusRule from its namespace ([ab298a7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ab298a72f56e8ce08cfc6a64fec79eaeb59c0dff))
* publish stable version ([f0f0349](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f0f0349c6b1f98d6493c6091dd5bc48f153ffd48))
* publish testing version ([a73227d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a73227de6ebb79500728271895e289a2f480b8b7))
* **socle:** backup and restore minio ([393fb21](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/393fb2120d0fba90450cb9dd04c868508b5a4546))
* **socle:** cleaning ingress controller values - upgrade version ([d89083c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d89083c153b2e95bff54433f298394284f79fea5))
* **socle:** optionnal ingress deployment ([7d38915](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7d38915c5e3a97dbf08b742c343541697dbdca18))
* update chart-version and appversion for gitea ([ba2059f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ba2059ff68a011b41d693c8914e3887007b04d90))
* update chart-version and appversion for kubernetes-dashboard ([f063566](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f06356606e211c7f95699dc256d4cf6b4a2a3f2f))
* update chart-version and appversion for nextcloud ([711985f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/711985f1c32616ccc97937684a84b64ce404351f))
* update to app version 1.8.0 ([bc687af](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bc687af8768f5099a46c5d424d61e7b943eae7c1))
* update to chart 3.5.3 and clean nextcloud values.yaml ([c09c356](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c09c356e80ea9ec1c0dbe7a4375849b50cae40d1))
* use upstream-chart-url variable for all charts ([2c4dfb6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2c4dfb6cd3df2c33788e3886f8f06c5a83c3e7d6))
* use vars.ini from repo as default config files ([cae63ac](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cae63acf005c79aece909eb3f3a4a92ef9107ff1))


### Bug Fixes

* activate redis by default for nextcloud addon ([9eeb1d3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9eeb1d343268a16db85ac6ab336ac8a98ad1f963))
* add chart-version to deploy script ([efa04bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/efa04bffc496b07d428c1804d99aa85ba3365761))
* add mastodon to readme file ([8a12736](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8a12736d15dfbdd499f61f8e4a8130f8589a155c))
* add questionnaire to sso valid redirect uri ([50c1bf6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/50c1bf63f1c110b4394bb3d3afc311eba3570c96))
* **addon:** add http redirect uri for codimd ([1015b8b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1015b8b1c55b748a0186d119e7ef50b95fbd4b82))
* **addon:** ugrade limits to avoid oom kill on rollout ([eb962f8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/eb962f8fe17ec40bd19f824806a07a14587cd6a4))
* **admin-tool:** fix loki deployement scripts ([c1d8933](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c1d89337d83546e882a0eb29b2f03590f36e5a4b))
* **admin-tool:** loki readme give correct datasource url ([3a68b9e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3a68b9e122b63f3725e8aaf467d3363d770e2860))
* all scripts should use log facility ([e34e6d6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e34e6d68ca0499ee81256adb9bd895621f258387))
* clean excalidraw values.yaml ([e785686](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e785686e994a5e6390daa36dc884f24769e43d28))
* clean filepizza values.yaml ([8a0be61](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8a0be61f66e85b60596905aa79b4e32b2d710f10))
* clean gitea values.yaml ([c8fa605](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c8fa605fc255e82fb1d5bbd12bbc6bb7e6e878e5))
* clean latelier values.yaml and correct lateliers-vars.ini ([75d3b8f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/75d3b8f5683966b0cf475636dff6d3152deaf062))
* clean rocketchat values.yaml ([d8b1c59](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d8b1c5954cf1d7ea7467b3ab3ee0e240a45f4fe4))
* clean screego values.yaml ([09b53e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/09b53e1a147a958959af99571c752083281da214))
* clean wikijs values.yaml ([bdace7a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bdace7ac0c596511f3631cb4c224f864327bc598))
* cleaning mobilizon values.yaml ([aa1153f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/aa1153f89a582b5f27aa83d477ea1919fb5ab2bf))
* cleaning values.yaml and drawio-vars.ini ([6474bee](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6474bee915760f5bf3be6ca53f269927b21885fb))
* correct include for log utility ([f079e75](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f079e754ec0e5bade94b485dc9d36cd31882a6b2))
* correct typo in namespace creation ([9b970e8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9b970e8436d496af1b36226c319c5a11f447dd1c))
* delete backup cronjobs only if disabled ([4c4c516](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4c4c516f5f94a9b6335a8684ca99bc6469c63c07))
* duplicate bitnami repo add removed ([51b30c2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/51b30c21dd1c53a328e5f24b0796112d2350a9c8))
* duplicate bitnami repo add removed ([c8f86c0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c8f86c0980f7845eedfc9d4d2a82c49a679afba4))
* duplicate bitnami repo add removed ([6035dd6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6035dd69a9c2d219121ed509efb991ca7f662494))
* increase nextcloud resources ([1ec1a05](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1ec1a0574e5a7cb41883e22a7fa27ae6e28b8bcc))
* internal database connection and required resources ([ae82efe](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ae82efeacfbff076f5580495ee50fd7b50ee7776))
* **mastodon:** helm chart doesn't seem to support --wait ([e1819a5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e1819a55a2cadb795aaac660d1b4376651dfecde))
* **mastodon:** restart services after install to prevent backend errors ([89e9be8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/89e9be85d452c045f257432eb526d6ff75e877a2))
* **mastodon:** use namespace defined in vars.ini ([260cd35](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/260cd3567ca08a4a0539ab0a98353a1abeefc49e))
* metrics are disabled by default ([eef98c0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/eef98c088594ebd55e5884cb12de726ebe48ba68))
* **mobilizon:** add delay time ([a775cd5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a775cd530899dd73296e1dfbcd05a75a6a529088))
* **mobilizon:** fix postgis persistence for tools ([14ce2b0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/14ce2b0f5cb2f4486530d660df7fca701c5e11c8))
* **mobilizon:** remove rollout applied to mobilizon pod ([b3fceaa](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b3fceaa335b26181789b090c0ccf72beeb34f25e))
* no error when create an existing namespace ([c6335ca](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c6335ca983c107d831730b3d93ad36c29f2b0805))
* number of replicas for read, write, backend are customizable ([9c797f2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9c797f2ef98fb3f6cb68b53edfa64c74cc5f5cf9))
* organise include templates directory ([0d8653f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0d8653f77e6a0c69d922ff36992eff0278f2fdef))
* remove deprecated admin-tools ([9d57912](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9d57912aa15f4abb620ef3c7c3860381e16dc7d7))
* replace deprecated minio client commands ([c6cfc9a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c6cfc9a644a32e61a8e1bc78bbf865fe9c3513b3))
* **socle and addons:** metrics activation test are now correct ([123f4a9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/123f4a9d41fc1b3c946904c6ca67ffc24b8e2134))
* **socle:** add minio backup schedule parameters ([4d59ae3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4d59ae30a664ab7892e534d0af2ae421e8c7a3d1))
* **socle:** create minio-backup-cronjob if enabled ([9d72c82](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9d72c822799e3338a9dc2541ac02081d01426587))
* **socle:** force single destination backup in demo mode ([8efa5d7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8efa5d73fffcf6637ae9fae66269aa23652a6255))
* **socle:** minio backup-restore in demo mode not in date subdirectory ([9b6b4ba](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9b6b4ba7116411bb99e613ae25d4f52e22d78326))
* **socle:** rename nginx section to ingress-nginx in cluster-vars.ini ([6c6471d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6c6471d0c3f4bf8708d4565590b9b706d25b303d))
* **socle:** split mount-backups in three pods ([cc9f175](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cc9f1757f37300d0220bf967499a22e674371063))
* **socle:** use stable image clients_tools for backup and restore ([0683dcf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0683dcfdc995f9c01d421f8cb47e57791d624876))
* update vars in meteor-settings.json ([a9c96bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a9c96bf30116869dc0f7fdff1d38f7fe35b091d9))
* update vars.ini and corrections in deploy script ([660f49d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/660f49d95b4780fcb44512d53180ae694f79fe92))


### Continuous Integration

* **build+test:** do not execute on draft commits ([4b6aeef](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4b6aeefcd83b7e0b799fb389c13f4d8f5b9c7a8a))

### [1.0.2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/1.0.1...release/1.0.2) (2023-02-02)


### Bug Fixes

* **socle:** restore keycloak database without postgres admin user ([9df4d8c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9df4d8c882b6f1cb10ca3045f510a4944dcb055f))

### [1.0.1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/compare/release/1.0.0...release/1.0.1) (2023-02-01)


### Bug Fixes

* **socle:** add postgres admin dbname for keycloak restore job ([6fbe840](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6fbe8402b807426695694fa2fca43a2492b9a711))
* **socle:** create backup directory if not exists ([9f01139](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9f01139f75debe53ce5af4b7583b6a498cf9d7f4))
* **socle:** use env vars instead of pg_dump options ([ad11b77](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ad11b775ffc2dcc5aea162d1ae225445a75c3bcf))

## 1.0.0 (2023-02-01)


### Features

* add drawio service to addon ([05e071e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/05e071e94c95563d70343d240eb1aa5d0fbb4ec7))
* add screego addon ([0b1d94c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0b1d94c89b9942b5ba46f4bd258fefa7376cbfc9))
* add service filepizza ([0e2d557](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0e2d557a1ceea5df783f3755e4681f7556a782b6))
* **addons:** factorize init-keycloak of addons ([728670d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/728670dfc94a1d6822b91e88f05cec23ea2053de))
* **admin tools:** add command to generate admin tools installation files ([8cac909](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8cac909b3dd06f503571a811cd4717aed7d38845))
* **admin tools:** move supercrud from addons to admin tools ([e6261f6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e6261f6ecd6aaeb5107f42a0c39df8c49bd2bafc))
* **admin-toolt:** add admin tool documentation ([8b675bf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8b675bfdc4b0f1c5b07696f71a7041baa41bbc25))
* **agenda:** use registry defined in vars.ini ([137ed06](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/137ed06799fbfd399c8b98fc784763ebb3c6d11e))
* **blog:** use registry defined in vars.ini ([894aad2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/894aad259e0e6ce19f8417a6d6f98eb46f743614))
* **build:** add setuptools integration ([13fc97f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/13fc97fd0f0b43590b740568ebf650b81c57bbde))
* **build:** don't create jinja loader and environment per templates ([4b578e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4b578e1c7111f5ed2bff4b0e9712bacd7e79dc77))
* **build:** jinja includes must always find `include` directories ([93e2ea0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/93e2ea0197bf3fd34921b3a24618641db544bcd1))
* cert-manager http01 ingress solver can be used ([88b6ef3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/88b6ef30cbeabc15c26baa3a2531d23da42b04fe))
* deploy stable version ([108c014](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/108c0148291e71ee77890b10fbaa181954721f17))
* **deploy:** create a new empty realm if `realmImport` is `false` ([4ff69a3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4ff69a3f70ca3b5257f62a73779b649d5de475b8))
* **deploy:** generate keycloak configuration secret only for import ([b868ae0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b868ae05785d0f66acea692b0b9d3011dcd03577))
* **deploy:** initialise keycloak before installing `laboite` ([3cff8b4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3cff8b44cc9b8d6212e17e9781c795dc0f150ba2))
* disable telemetry by default ([7ce1f9c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7ce1f9c74a324399aaef01cf993230dbbc055d9e))
* finalize wikijs installation ([34ec01f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/34ec01fc1e1a184b7002b859457de3332c4d1e90))
* **frontal-nextcloud:** use registry defined in vars.ini ([142d69e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/142d69eddf604b7fa1f6816ad90557ace3c884ca))
* **gen-socle:** add possibility to not install keycloak or laboite ([722a932](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/722a9324c98e9885c01a5ce2771097e260c54798))
* **grafana:** add grafana admin tool ([2f93432](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2f93432eb87304a304bdb76a940ea4ae5d84907c))
* **ingress-nginx:** upgrade to helm version 4.2.5 ([0b3c347](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0b3c347bd592105d43452cfef734bb70a0b2196e))
* **ingress-nginx:** user can force `loadBalancerIP` value ([c205120](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c20512033406630c1cdb20a512803ff89762af01))
* **keycloak:** import values are enabled only when configured ([235d179](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/235d179cbc738712d47b56332aa041b046cd2139))
* **keycloak:** use registry defined in vars.ini ([bc0f9f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bc0f9f40d75d3e164709c97fbcdee082794a0c48))
* **kubernetes-dashboard:** add a service account token for viewer ([e76a3b1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e76a3b12c4cab11a46aebe55e3f96d621ffb5dfd))
* **kubernetes-dashboard:** add admin account ([d2534cd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d2534cdb51869dcfa23db78e6febc8e31283047b))
* **kubernetes-dashboard:** add ingress annotations and hosts ([6822958](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/682295888ce0b411906c6342047089274efa282c))
* **kubernetes-dashboard:** add kubernetes dashboard admin tool ([b530945](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b53094591e598fdebcec660846bd9a122e6c9b1f))
* **kubernetes-dashboard:** add undeploy script ([a043ff8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a043ff8f6c20f47504fbb00e3f84e9e4ae82b9fb))
* **kubernetes-dashboard:** add viewer ([4c2e201](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4c2e2012cdbfc69d7cde630c5ccd75d19ab01521))
* **kubernetes-dashboard:** configure resource requests and limits ([c2f11bd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c2f11bd7e01c8081184408b2d1e56c4e0d16c193))
* **laboite:** add nexcloud parameters in meteor settings ([9db924d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9db924ddcbd7cdc90f07b437c25ef48f78959e19))
* **laboite:** use registry defined in vars.ini ([9daab69](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9daab69732315302d5cac3d8d6ecc11be46fb6df))
* **loki:** add loki admin tool ([ecaea01](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ecaea0101cf8aa683c6f6ff726cd658d84cbda4d))
* **loki:** use the ingress ([6e581ba](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6e581bab9b03dd9c4b86afe41e6673dd12ba1c33))
* **lookup-server:** use registry defined in vars.ini ([8a26d16](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8a26d16f0b7ac3b858575bd4430f34b2b4839a64))
* **mezig:** use registry defined in vars.ini ([3032e40](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3032e401736d3bed9b5c7ad0df4105f44f0d0f69))
* **mongodb:** Enable Prometheus metrics exporter ([4134090](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/41340902c8975883d22f94095a923297863e1fcc))
* permit to choose cluster-issuer name and certificate type ([bebfe4c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bebfe4c72b46bd7205446871313b9f98c94f13ba))
* permit to use already installed cert-manager ([a565e8e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a565e8ec70e7c3a420160cec4af430c707176aeb))
* **prometheus:** Add prometheus admin tool ([26395fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/26395fc099d2788561dd30c9762e4374766f91f0))
* **promtail:** add promtail admin tool ([252863f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/252863fa720fceb4c57a979219acc53d2f9dace6))
* **promtail:** customize client URL (loki) ([7b399e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7b399e143c0ca0a724e0dde9331fdd9128b558df))
* **radicale:** use registry defined in vars.ini ([b8b6864](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b8b68640706958c4e8c51a66fe4d78c1ea86af1a))
* **readme:** add latest addons ([5f85f42](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5f85f4272592d91d2b4269b4f051e47f7a0c3d9f))
* rocketchat can be installed in its own namespace ([d955d14](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d955d14f03a2fa189796fe4192faaa823e9b8b20))
* rocketchat does not show wizard on first run ([16d4d94](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/16d4d947dbf2e649886bbfe70e51a6fd9a0c660b))
* **socle:** build redirecturis for keycloak sso client ([cbd3fcd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/cbd3fcdaadb789c30e333cb6efa2cc02e3f74bdf))
* **socle:** correct and improve redirecturis generation ([3c05c75](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3c05c75a9380b5db9d466014811c623b666bf31d))
* **socle:** gen-socle generates scripts to deploy 1.2.0 laboite chart ([4c82322](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4c823227b42489bb839d25914801107cd9b86062))
* **socle:** implement keycloak backup and restore procedure ([2573700](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2573700623b83575a0ac816dae262a0a7056264e))
* **socle:** implement mongodb backup and restore procedure ([31c0236](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/31c023687001939a1a6f915dc1f171ce4f4b966b))
* **sondage:** use registry defined in vars.ini ([38dff93](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/38dff935aa66f2be6fe6aa7d899980701620cf96))
* update supercrud admin tool ([bdcb7c4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bdcb7c4fd6601e3dfc750ca1ea82478e10af831a))
* upgrade minio version ([978409b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/978409b7190d246cacdf7d8f54dbaba9644a53da))
* upgrade mongodb chart and app version ([630643b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/630643b2f102594072547e15692cfcd7dc9b73c7))
* **vars.ini:** define default registry ([2c59fbf](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2c59fbff1175f55775351d81c6596453685654f8))
* **vars.ini:** import realm by default ([bd60cf6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bd60cf68841f182bdf8cd80f45508e3c9b0cab6c))


### Bug Fixes

* add missing parameters for blog and blogapi ([4c4f80d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/4c4f80d6871ee95fab3e39aa665dc3f66cc1707b))
* **addon:** add chart-version in build and test scripts ([adca9a9](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/adca9a98c0119df76e99adb5daf7fc2d139690fc))
* **addon:** add chart-version to excalidraw addon scripts ([d600a36](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/d600a369603032f158ae54fba5cd664b9de728d9))
* **addon:** add chart-version to wikijs addon deploy and test scripts ([a5466fb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a5466fbc510e0476a080765d64a8c5c8345b22c2))
* **addon:** add demo mode for gitea addon ([475f810](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/475f810ab971b07bc2a47c529deb5d128f15d856))
* **addon:** gen-addon command can be execute outside tools directory ([1e69641](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1e69641a2a1912149ff47d63ee9667d327835cff))
* **addon:** set correct helm repo for filepizza ([124c5d4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/124c5d47926f1e07e105e537a3b274b426c052a8))
* **addon:** set filepizza chart version ([0cb8e02](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0cb8e0275385fade4d414df5b93b188fad26acfc))
* **admin-tool:** fix loki chart-version ([480a881](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/480a881f1cb9fa7bc51ce5993348c9faa583a773))
* **admin-tool:** promtail add chart-version to scripts ([f6c3f09](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f6c3f0916b296425fb39574056f1c01b80456420))
* **admin-tool:** set correct dns service name for loki ([78b4ec5](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/78b4ec5aa1ee9bb090e24e492d3bd22e93a00cdf))
* **admin-tools:** internal minio for loki supports demomode ([e922b8c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e922b8c945c2eb5343072f27125f45993022a031))
* **build:** add laboite parameters ([0a35ede](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/0a35edec7ec767a0287f83f6ef6ad74fb1fe1f55))
* **build:** display correct output directory in readme ([7a9b067](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7a9b067c893143aa470a7f2b35c57bf8b4e641be))
* **build:** does not run outside of the `tools/` directory ([7503985](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/7503985d5c783330e29c152ff86974d500566796))
* codimd can be installed in its own namespace ([f5c04cb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f5c04cbc86d9441c04a982fe71da6dacccdaff69))
* **deploy:** use nginx's chart name in its release name ([05b97eb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/05b97ebe3df58f181a04968dc2b803622c62abbf))
* **filepizza:** add chart version to filepizza ([1a77eb1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/1a77eb1624a5125db95668209026144f711afcab))
* ignore make minio bucket if already exists ([fae7104](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/fae7104658908c1f7e33df6818a7ca6204cd0569))
* **ingress-nginx:** upgrade chart version to 4.2.5 ([3c5be04](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3c5be04a77e31afcb344c871deed01e5958f2cd2))
* installation don't fail if users already present ([f045101](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/f045101495efc766a73eef0d4198da5320e4d465))
* **keycloak:** cleaning keycloak values template ([c5a7b84](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c5a7b84fba0238e15651cbd7fbddccb53b12f411))
* **keycloak:** jinja includes are relative to `templates` directory ([bca6181](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/bca6181472588eee03b0c4b2cf1c7c78cf44b7c1))
* **kubernetes-dashboard:** prevent mixup beween go template and jinja ([8fd43ab](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8fd43ab26ccadf9351f0149a5fefa96e2a8d5e70))
* **loki:** disable authentication ([3cb45fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3cb45fc2bc9be78426a2df1df48072a3a062bcac))
* **loki:** use local minio ([ea077ad](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ea077ade6b0440687727d6b14fa47d374430f0fc))
* **minio:** cleaning minio values ([6210ce2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/6210ce2e64dab93f8104e498ca7fd4330ad9635f))
* **mongodb:** update chart version and clean values file ([e63cef8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e63cef80bd2a28b8327e7cf88b0df43c2171a53f))
* parsing configuration file fails when a percent is present ([a46cb19](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a46cb1949e2e7d456e8fcdad875a70884ff54520))
* **prometheus:** add missing helm repo ([c9bbcff](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c9bbcffccccea0e1b94c78bcc4762fba52c4114f))
* **prometheus:** missing helm repository ([afd72e1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/afd72e1108334b11883b0ef7a6e3dd1eaa156067))
* **radicale:** add missing parameters ([9215acd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/9215acd175785d9c1ba892f34d658c4557c923be))
* removed unused boolean var for resources ([48435b4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/48435b4c1e7cae63de91835f2944e98f5aae7e81))
* restore vars.ini before bad conflict resolution ([e6e3d6c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e6e3d6c8cc68ab99b4268a27766fd1040f129a37))
* **socle:** add replicacount parameters for all socle services ([685d0d7](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/685d0d79fcff5fe9421acb7c5fc02ba69a319c07))
* **socle:** configure demo mode for minio ([59e93f6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/59e93f69f762ef5635cb1cd13410eab55ba22eda))
* **socle:** fix metrics tests in keycloak values template ([dd9d7da](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/dd9d7da68bda1a119d8889cf52784ff788c0e0b0))
* **socle:** generate radicale keycloak client creation ([486da0b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/486da0b3c286e037d4dba79d1f9ac3868a11e39d))
* **socle:** include keycloak metrics events only if enabled ([2c7e5de](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2c7e5de3e8df3b11d6d4acf394738944ad8f78e3))
* **socle:** init-keycloak includes only one file at a time ([90943fc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/90943fcbbfcae9b911842f2e3c1ed47c18b860b8))
* **socle:** testing tools version must deploy testing charts versions ([a74f0e2](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/a74f0e221256338e605d83e13252031fb21329bb))
* update ingress configuration. ([82198ad](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/82198adb0278024087d513c8fbd6362980086e39))
* update kubernetes-dashboard ingress for cert-manager ([3099043](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3099043c0257f0f5c1df8596774782c79da29bc2))
* update loki ingress for cert-manager ([197915d](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/197915d16c084023f2cf69790f8b645412aad386))
* update mongodb grafana dashboard ([44149fb](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/44149fbfc4fef3a537cc869e70a99cc99dfd346f))
* update prometheus-stack ingress for cert-manager ([80c44e6](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/80c44e619d12996040785ecb869a27680f13a320))
* **utils-log.sh.j2:** jinja includes are relative to `templates/` ([61ec0df](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/61ec0dfcfa33976501a841b9cfdeba33d81e04d9))
* **values:** add nextcloud values for user, password and quota ([8e355ea](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/8e355ea7b9b7c78cecd1ebebc5de5a47df6a52be))


### Code Refactoring

* **build:** template generation is always the same logic ([5d68b9b](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/5d68b9be789c889aca5696f2f34dac4d6ddec944))


### Styles

* **build:** one import per line from generic to specific ([3ec7579](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3ec75796ddde82d678f2fc4d8ad67bbf92e374c3))
* **build:** reformat and add missing function documentation ([75f234e](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/75f234e4550900f1dfced4466ea173d43cde8db7))
* **build:** reformat with python black ([ad5ded0](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/ad5ded00249eeff5013b20c6527aa152b9359775))
* **build:** use f-strings instead of `.format()` ([2ddaa1c](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/2ddaa1ca137772cffab9747d8d62db3e9fb289ad))
* **include:** relative path is useless ([e7e9efc](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e7e9efc0a113048d011a4a76d197afdb04b05f76))


### Documentation

* **contributing:** explain commit message formatting ([c417391](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/c4173915924e4f0af99d23ac5667fd16565eea35))


### Continuous Integration

* **addon:** add filepizza addon ([09c42ee](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/09c42ee5ca74f4657554d95c354334421113e7f5))
* **build:** generate all addons ([b11ead1](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/b11ead161732db9b3fcd218d2219407a22213376))
* **build:** generate all admin tools ([800f3f4](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/800f3f41d93103e22b0bdae66213ad948ee7a279))
* **build:** generate socle ([07ce1fd](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/07ce1fd58fc1db80c1c6cb9683588b01d6c9d230))
* **chart:** test socle charts templates ([e6b7c0f](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e6b7c0f82979c10fb3127cd7c15f3d8d7a14b2af))
* **helm-template:** test addon chart template ([81f7398](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/81f73985d0acb25e610615a650cb0c043d10e2a1))
* **helm-template:** test admin-tools chart template ([99e2676](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/99e2676b1b7b0961e1b409422aafa91f323efa2f))
* **initial-checks:** enforce git commit message formatting ([88e8715](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/88e8715370b71c61f01dac90027a6902f4ead2e1))
* **lint:** enforce python black formatting of build script ([3b6b651](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/3b6b651d3d0f087912b2b91222d5e625c12fdddc))
* **release:** avoid regression in `dev` branch ([e30036a](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/e30036ad0321ca144c168b56fd9f2a6fa06ca399))
* **release:** create `testing` and `stable` releases ([20c3ba8](https://gitlab.mim-libre.fr/EOLE/eole-3/tools/-/commit/20c3ba8b7c33b5f36dab3bf3e7c120e57023f694))
