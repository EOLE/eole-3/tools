# -*- coding: utf-8 -*-

__version__ = "10.0.0"

import click

from .commands.build import build
from .commands.backup import backup
from .commands.restore import restore
from .commands.config import config
from .commands.deploy import deploy
from .commands.update import update
from .commands.mcm import mcm
from .commands.maintenance import maintenance
from .commands.show import show
from . import options
from . import data


class OrderCommands(click.Group):
    def list_commands(self, ctx: click.Context) -> list[str]:
        return list(self.commands)


@click.group(cls=OrderCommands)
@options.config
@click.version_option(__version__)
@click.pass_context
def cli(ctx, config):
    ctx.ensure_object(dict)
    ctx.obj["default_config_files"] = data.default_socle_config_files
    ctx.obj["custom_config_files"] = list(config)


cli.add_command(config)
cli.add_command(mcm)
cli.add_command(maintenance)
cli.add_command(build)
cli.add_command(backup)
cli.add_command(restore)
cli.add_command(deploy)
cli.add_command(update)
cli.add_command(show)

if __name__ == "__main__":
    cli()
