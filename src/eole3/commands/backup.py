# -*- coding: utf-8 -*-

import subprocess
from os.path import join

import yaml
import click

from .. import options
from .. import data
from .. import utils


@click.group()
@options.output_dir
@click.pass_context
def backup(ctx, outputdir):
    """
    Backup tools for EOLE3
    """
    ctx.ensure_object(dict)
    ctx.obj["outputdir"] = outputdir


@backup.command()
@options.backup_actions
@click.pass_context
def socle(ctx, action):
    """
    Perform backup actions for socle deployments
    """
    cwd = ctx.obj["outputdir"]
    process = execCmd(action, cwd)


@backup.command()
@options.component(type="infra")
@options.backup_actions
@click.pass_context
def infra(ctx, action, name=None):
    """
    Perdorm backup actions for infra components
    """
    cwd = join(ctx.obj["outputdir"], "infra", name)
    process = execCmd(action, cwd)


@backup.command()
@options.component(type="addons")
@options.backup_actions
@click.pass_context
def addon(ctx, action, name=None):
    """
    Perform backup actions for addon components
    """
    cwd = join(ctx.obj["outputdir"], "addons", name)
    process = execCmd(action, cwd)


@backup.command()
@options.component(type="ladigitale")
@options.backup_actions
@click.pass_context
def ladigitale(ctx, action, name=None):
    """
    Perform backup actions for Ladigitale addons
    """
    cwd = join(ctx.obj["outputdir"], "ladigitale", name)
    process = execCmd(action, cwd)


@backup.command(name="admin-tool")
@options.component(type="admin-tools")
@options.backup_actions
@click.pass_context
def admin_tool(ctx, action, name=None):
    """
    Perform backup actions for admin tool component
    """
    cwd = join(ctx.obj["outputdir"], "admin-tools", name)
    process = execCmd(action, cwd)


###
def execCmd(action, cwd=None):
    action_scripts = {
        "list": "list-backup.sh",
        "save": "backup.sh",
    }
    script = action_scripts[action]
    utils.check_commands()
    try:
        subprocess.run(
            ["ls", script],
            cwd=cwd,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            check=True,
        )
        return subprocess.run(["bash", script], cwd=cwd)
    except:
        print("No action available for this component")
