# -*- coding: utf-8 -*-

import subprocess
from os.path import join

import yaml
import click

from .. import options
from .. import data
from .. import utils


@click.group()
@options.output_dir
@click.pass_context
def deploy(ctx, outputdir):
    """
    Deploy generated templates
    """
    ctx.ensure_object(dict)
    ctx.obj["outputdir"] = outputdir


@deploy.command()
@options.socle_component()
@click.pass_context
def socle(ctx, name="all"):
    """
    Deploy socle services and infrastructure components
    """
    if name == "all":
        deployInfraComponents(ctx)
    cwd = ctx.obj["outputdir"]
    process = execCmd(cwd)


@deploy.command()
@options.component(type="infra")
@click.pass_context
def infra(ctx, name=None):
    """
    Deploy infra
    """
    cwd = join(ctx.obj["outputdir"], "infra", name)
    process = execCmd(cwd)


@deploy.command()
@options.component(type="addons")
@click.pass_context
def addon(ctx, name=None):
    """
    Deploy addon
    """
    cwd = join(ctx.obj["outputdir"], "addons", name)
    process = execCmd(cwd)


@deploy.command()
@options.component(type="ladigitale")
@click.pass_context
def ladigitale(ctx, name=None):
    """
    Deploy Ladigitale app
    """
    cwd = join(ctx.obj["outputdir"], "ladigitale", name)
    process = execCmd(cwd)


@deploy.command(name="admin-tool")
@options.component(type="admin-tools")
@click.pass_context
def admin_tool(ctx, name=None):
    """
    Deploy admin tool
    """
    cwd = join(ctx.obj["outputdir"], "admin-tools", name)
    process = execCmd(cwd)


###
def deployInfraComponents(ctx, app_type=None, name=None):
    if app_type != None:
        app_dir = data.eole3_data.joinpath(app_type, name)
        components_file = app_dir.joinpath(data.infra_components_file)
    else:
        components_file = data.eole3_data.joinpath(data.infra_components_file)
    with open(components_file, "r") as infra:
        components = yaml.load(infra, Loader=yaml.FullLoader)["components"]
    for name in components:
        cwd = join(ctx.obj["outputdir"], "infra", name)
        process = execCmd(cwd)


###
def execCmd(cwd=None):
    utils.check_commands()
    try:
        return subprocess.run(["bash", "deploy"], cwd=cwd)
    except Exception as e:
        print("deploy command error :", e)
