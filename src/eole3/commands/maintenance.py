# -*- coding: utf-8 -*-

import subprocess
from os.path import join

import yaml
import click

from .. import options
from .. import data
from .. import utils


@click.group()
@options.output_dir
@click.pass_context
def maintenance(ctx, outputdir):
    """
    Redirect ingress on maintenance page
    """
    ctx.ensure_object(dict)
    ctx.obj["outputdir"] = outputdir


@maintenance.command()
@options.maintenance_component(type="socle")
@click.pass_context
def socle(ctx, name=None):
    """
    Redirect ingress on maintenance for a socle component
    """
    cwd = ctx.obj["outputdir"]
    process = execCmd(cwd, name)


@maintenance.command()
@options.maintenance_component(type="infra")
@click.pass_context
def infra(ctx, name=None):
    """
    Redirect ingress on maintenance for an infra component
    """
    cwd = join(ctx.obj["outputdir"], "infra", name)
    process = execCmd(cwd, name)


@maintenance.command()
@options.maintenance_component(type="addons")
@click.pass_context
def addon(ctx, name=None):
    """
    Redirect ingress on maintenance for an addon component
    """
    cwd = join(ctx.obj["outputdir"], "addons", name)
    process = execCmd(cwd, name)


@maintenance.command()
@options.maintenance_component(type="ladigitale")
@click.pass_context
def ladigitale(ctx, name=None):
    """
    Redirect ingress on maintenance for ladigitale component
    """
    cwd = join(ctx.obj["outputdir"], "ladigitale", name)
    process = execCmd(cwd, name)


@maintenance.command(name="admin-tool")
@options.maintenance_component(type="admin-tools")
@click.pass_context
def admin_tool(ctx, name=None):
    """
    Redirect ingress on maintenance for admin tool component
    """
    cwd = join(ctx.obj["outputdir"], "admin-tools", name)
    process = execCmd(cwd, name)


###
def execCmd(cwd=None, name=None):
    utils.check_commands()
    try:
        return subprocess.run(["bash", "maintenance", name], cwd=cwd)
    except Exception as e:
        print("maintenance command error :", e)
