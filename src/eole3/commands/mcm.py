# -*- coding: utf-8 -*-

from os import makedirs
from os.path import basename
from os.path import dirname
from os.path import isfile
from os.path import isdir
from os.path import join
from os.path import relpath

from jinja2 import Environment
from jinja2 import FileSystemLoader

import click
import io

from .. import options
from .. import data
from .. import utils


@click.group()
@options.config
@click.pass_context
def mcm(ctx, config):
    """
    Generate minimized configuration file with only non-default values
    """
    ctx.ensure_object(dict)
    ctx.obj["custom_config_files"].extend(list(config))


@mcm.command()
@options.config
@options.output_file
@click.pass_context
def socle(ctx, config, outputfile):
    """
    Minimize socle configuration file
    """
    ctx.obj["custom_config_files"].extend(list(config))
    ctx.obj["outputfile"] = outputfile
    gen_diff_config(ctx)


@mcm.command()
@options.component(type="infra")
@options.config
@options.output_file
@click.pass_context
def infra(ctx, config, outputfile, name=None):
    """
    Minimize infra configuration file
    """
    ctx.obj["custom_config_files"].extend(list(config))
    ctx.obj["outputfile"] = outputfile
    gen_diff_extra_config(ctx, "infra", name)


@mcm.command()
@options.component(type="addons")
@options.config
@options.output_file
@click.pass_context
def addon(ctx, config, outputfile, name=None):
    """
    Minimize addon configuration file
    """
    ctx.obj["custom_config_files"].extend(list(config))
    ctx.obj["outputfile"] = outputfile
    gen_diff_extra_config(ctx, "addons", name)


@mcm.command()
@options.component(type="ladigitale")
@options.config
@options.output_file
@click.pass_context
def ladigitale(ctx, config, outputfile, name=None):
    """
    Minimize ladigitale configuration file
    """
    ctx.obj["custom_config_files"].extend(list(config))
    ctx.obj["outputfile"] = outputfile
    gen_diff_extra_config(ctx, "ladigitale", name)


@mcm.command(name="admin-tool")
@options.component(type="admin-tools")
@options.config
@options.output_file
@click.pass_context
def admin_tool(ctx, config, outputfile, name=None):
    """
    Minimize admin-tool configuration file
    """
    ctx.obj["custom_config_files"].extend(list(config))
    ctx.obj["outputfile"] = outputfile
    gen_diff_extra_config(ctx, "admin-tools", name)


def gen_diff_extra_config(ctx, app_type, name):
    """
    Generate minimized confif file for any extra tool (`addon` or `admin-tool`)
    """
    extra_dir = data.eole3_data.joinpath(app_type, name)

    ctx.obj["default_config_files"].append(extra_dir.joinpath(f"{name}-vars.yaml"))
    gen_diff_config(ctx, app_type)


def gen_diff_config(ctx=None, app_type="socle"):
    """
    Write minimized configuration file
    """
    config_files = ctx.obj["default_config_files"]

    default_config = utils.merge_config(config_files=config_files)

    config_files.extend(ctx.obj["custom_config_files"])

    custom_config = utils.merge_config(config_files=config_files)

    diff_config = my_config_minimised(default_config, custom_config)

    utils.yaml.dump(diff_config, ctx.obj["outputfile"])


def my_config_minimised(d1, d2):
    if isinstance(d1, dict):
        diff = {}
        for k in d2.keys():
            if k in d1.keys():
                if isinstance(d2[k], dict):
                    result = my_config_minimised(d1[k], d2[k])
                    if result != {}:
                        diff[k] = result
                elif isinstance(d2[k], list):
                    for elem in d1[k]:
                        idx = d1[k].index(elem)
                        if isinstance(elem, dict):
                            result = my_config_minimised(d1[k][idx], d2[k][idx])
                            if result != {}:
                                diff[k] = result
                        else:
                            if d1[k][idx] != d2[k][idx]:
                                diff[k] = d2[k]
                else:
                    if d1[k] != d2[k]:
                        diff[k] = d2[k]
            else:
                diff[k] = d2[k]
        return diff
