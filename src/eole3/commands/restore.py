# -*- coding: utf-8 -*-

import subprocess
from os.path import join

import yaml
import click

from .. import options
from .. import data
from .. import utils


@click.group()
@options.output_dir
@click.pass_context
def restore(ctx, outputdir):
    """
    Restore tools for EOLE3
    """
    ctx.ensure_object(dict)
    ctx.obj["outputdir"] = outputdir


@restore.command()
@options.restore_day
@click.pass_context
def socle(ctx, day):
    """
    Restore the 'day' backup
    """
    cwd = ctx.obj["outputdir"]
    process = execCmd(day, cwd)


@restore.command()
@options.component(type="infra")
@options.restore_day
@click.pass_context
def infra(ctx, day, name=None):
    """
    Restore the 'day' backup for infra components
    """
    cwd = join(ctx.obj["outputdir"], "infra", name)
    process = execCmd(day, cwd)


@restore.command()
@options.component(type="addons")
@options.restore_day
@click.pass_context
def addon(ctx, day, name=None):
    """
    Restore the 'day' backup for addon components
    """
    cwd = join(ctx.obj["outputdir"], "addons", name)
    process = execCmd(day, cwd)


@restore.command()
@options.component(type="ladigitale")
@options.restore_day
@click.pass_context
def ladigitale(ctx, day, name=None):
    """
    Restore the 'day' backup for Ladigitale addons
    """
    cwd = join(ctx.obj["outputdir"], "ladigitale", name)
    process = execCmd(day, cwd)


@restore.command(name="admin-tool")
@options.component(type="admin-tools")
@options.restore_day
@click.pass_context
def admin_tool(ctx, day, name=None):
    """
    Restore the 'day' backup for admin tool component
    """
    cwd = join(ctx.obj["outputdir"], "admin-tools", name)
    process = execCmd(day, cwd)


###
def execCmd(day, cwd=None):
    utils.check_commands()
    try:
        subprocess.run(
            ["ls", "restore.sh"],
            cwd=cwd,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            check=True,
        )
        return subprocess.run(["bash", "restore.sh", day], cwd=cwd)
    except:
        print("No backup available for this component")
