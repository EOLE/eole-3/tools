# -*- coding: utf-8 -*-

from os.path import join

import click

from .. import data


@click.group(chain=True, invoke_without_command=True)
def show():
    """
    Show all available components and their types
    """
    pass


@show.result_callback()
def process_pipeline(processors):
    components = []
    for processor in processors:
        components.append(processor())

    if processors == []:
        for component_type in data.components_types:
            components.append({component_type: data.components[component_type]})

    display(components)


@show.command()
def infra():
    """
    Show all available infra components
    """

    def processor():
        return {"infra": data.components["infra"]}

    return processor


@show.command()
def addons():
    """
    Show all available addons components
    """

    def processor():
        return {"addons": data.components["addons"]}

    return processor


@show.command()
def ladigitale():
    """
    Show all available Ladigitale applications
    """

    def processor():
        return {"ladigitale": data.components["ladigitale"]}

    return processor


@show.command(name="admin-tools")
def admin_tools():
    """
    Show all available admintools components
    """

    def processor():
        return {"admin-tools": data.components["admin-tools"]}

    return processor


def display(components):
    for component in components:
        for key, value in component.items():
            click.echo(key + ":")
            click.echo("    " + " ".join(value))
