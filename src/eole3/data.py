# -*- coding: utf-8 -*-

eole3_data = None
try:
    from importlib.resources import files
    from os import listdir

    eole3_data = files("eole3").joinpath("data")
except ImportError as err:
    from importlib.resources import path

    with path("eole3", "data") as data_path:
        eole3_data = data_path

components_types = ["infra", "addons", "ladigitale", "admin-tools"]
components = {}
for component_type in components_types:
    components[component_type] = listdir(eole3_data.joinpath(component_type))

socle_components = ["all", "services-only"]

maintenance_components_types = ["socle", "infra", "addons", "ladigitale", "admin-tools"]
maintenance_components = {}
maintenance_components["socle"] = [
    "laboite",
    "agenda",
    "blog",
    "blogapi",
    "frontnxt",
    "laboite-api",
    "lookup-server",
    "mezig",
    "questionnaire",
    "radicale",
    "sondage",
]
maintenance_components["infra"] = ["keycloak", "minio"]
maintenance_components["admin-tools"] = ["kubernetes-dashboard", "grafana", "supercrud"]
maintenance_components["addons"] = listdir(eole3_data.joinpath("addons"))
maintenance_components["ladigitale"] = listdir(eole3_data.joinpath("ladigitale"))

infra_components_file = "infra_components.yaml"

default_socle_config_files = [
    eole3_data.joinpath("vars.yaml"),
]

templates_dir = eole3_data.joinpath("templates")
