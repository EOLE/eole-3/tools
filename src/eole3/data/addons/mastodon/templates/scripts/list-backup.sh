{%- from "include/postgresql/list-backup-script.j2" import list_postgresql_backup_script -%}

#!/bin/bash

BACKUP_NAMESPACE="backup"

{{ list_postgresql_backup_script(config, 'mastodon') }}
