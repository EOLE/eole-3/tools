{%- from "include/mariadb/backup-script.j2" import mariadb_backup_script as backup_script -%}

#!/bin/bash

{{ backup_script(config, 'matomo') }}
