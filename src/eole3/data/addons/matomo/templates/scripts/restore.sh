{%- from "include/mariadb/restore-script.j2" import mariadb_restore_script as restore_script -%}

#!/bin/bash

{{ restore_script(config, 'matomo', 'deployment') }}
