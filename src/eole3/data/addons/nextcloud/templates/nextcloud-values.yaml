{% from "include/postgresql/dbvars.inc" import load_dbvars %}
{%- set dbvars = load_dbvars(config=config, component='nextcloud') | from_json -%}
{%- from 'include/utils/s3-vars.j2' import load_s3vars %}
{%- set s3vars = load_s3vars(config, 'nextcloud') | from_json -%}
nameOverride: "{{ config['nextcloud']['hostname'] }}"
fullnameOverride: "{{ config['nextcloud']['hostname'] }}"


{%- if get_value(config, "nextcloud", "images:registry") != "upstream" %}
image:
  repository: {{ get_value(config, "nextcloud", "images:registry") }}/{{ get_value(config, "nextcloud", "images:nextcloud:name") }}
  {%- if get_value(config, "nextcloud", "images:nextcloud:tag") %}
  tag: {{ get_value(config, "nextcloud", "images:nextcloud:tag") }}
  {%- endif %}
{%- endif %}

{%- if get_value(config, "nextcloud", "images:registry") != "upstream" %}
metrics:
  image:
    repository: {{ get_value(config, "nextcloud", "images:registry") }}/{{ get_value(config, "nextcloud", "images:nextcloud-exporter:name") }}
    {%- if get_value(config, "nextcloud", "images:nextcloud-exporter:tag") %}
    tag: {{ get_value(config, "nextcloud", "images:nextcloud-exporter:tag") }}
    {%- endif %}
{%- endif %}

{%- if get_value(config, "nextcloud", "images:registry") != "upstream" %}
nginx:
  image:
    repository: {{ get_value(config, "nextcloud", "images:registry") }}/{{ get_value(config, "nextcloud", "images:nginx:name") }}
    {%- if get_value(config, "nextcloud", "images:nginx:tag") %}
    tag: {{ get_value(config, "nextcloud", "images:nginx:tag") }}
    {%- endif %}
{%- endif %}

## Allowing use of ingress controllers
## ref: https://kubernetes.io/docs/concepts/services-networking/ingress/
##
ingress:
  enabled: true
  # className: nginx
  className: {{ config['default']['ingress']['className']  }}
  annotations:
  {%- if config['cert-manager']['enabled'] %}
    cert-manager.io/cluster-issuer: "{{ config['cert-manager']['clusterIssuerName']|default('letsencrypt', true) }}-{{ config['cert-manager']['type'] }}"
  {%- endif %}
    nginx.ingress.kubernetes.io/proxy-body-size: 4G
    nginx.ingress.kubernetes.io/server-snippet: |-
      server_tokens off;
      proxy_hide_header X-Powered-By;

      rewrite ^/.well-known/webfinger /public.php?service=webfinger last;
      rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
      rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json;
      location = /.well-known/carddav {
        return 301 $scheme://$host/remote.php/dav;
      }
      location = /.well-known/caldav {
        return 301 $scheme://$host/remote.php/dav;
      }
      location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
      }
      location ~ ^/(?:build|tests|config|lib|3rdparty|templates|data)/ {
        deny all;
      }
      location ~ ^/(?:autotest|occ|issue|indie|db_|console) {
        deny all;
      }
  tls:
  - hosts:
    - {{ config['nextcloud']['hostname'] }}.{{ config['default']['domain'] }}
    {%- if config['cert-manager']['enabled'] %}
    secretName: tls-{{ config['nextcloud']['hostname'] }}
    {%- endif %}
  labels: {}
  path: /
  pathType: Prefix




nextcloud:
  host: {{ config['nextcloud']['hostname'] }}.{{ config['default']['domain'] }}
  username: {{ config['nextcloud']['adminUser'] }}
  password: {{ config['nextcloud']['adminPassword'] }}
  # Extra config files created in /var/www/html/config/
  # ref: https://docs.nextcloud.com/server/15/admin_manual/configuration_server/config_sample_php_parameters.html#multiple-config-php-file
  configs:
    log.config.php: |-
        <?php
        $CONFIG = array (
           'loglevel' => 0,
        );
    oidc.config.php: |-
        <?php
        $CONFIG = array (
           'oidc_login_client_id' => '{{ config["nextcloud"]["keycloak"]["clientName"] }}', // Client ID: Step 1
           'oidc_login_client_secret' => '{{ config["nextcloud"]["keycloak"]["clientSecret"] }}', // Client Secret: Got to Clients -> Client -> Credentials
           'oidc_login_provider_url' => 'https://{{ config["keycloak"]["hostname"] }}.{{ config["default"]["domain"] }}/auth/realms/{{ config["keycloak"]["realm"] }}',
           'oidc_login_logout_url' => 'https://{{ config["keycloak"]["hostname"] }}.{{ config["default"]["domain"] }}/auth/realms/{{ config["keycloak"]["realm"] }}/protocol/openid-connect/logout?redirect_uri=https%3A%2F%2F{{ config["nextcloud"]["hostname"] }}.{{ config["default"]["domain"] }}%2F',
           'oidc_login_auto_redirect' => true,
           'oidc_login_redir_fallback' => true,
           'oidc_login_disable_registration' => false,
           'oidc_login_attributes' => array(
                'id' => 'preferred_username',
                'mail' => 'email',
           ),
           // If you are running Nextcloud behind a reverse proxy, make sure this is set
           'overwriteprotocol' => 'https',
         );
    minio.config.php: |-
        <?php
        $CONFIG = array (
          'objectstore' => array(
            'class' => '\\OC\\Files\\ObjectStore\\S3',
            'arguments' => array(
              'bucket'     => '{{ s3vars.buckets | first }}',
              'autocreate' => false,
              'key'        => '{{ s3vars.accessKey }}',
              'secret'     => '{{ s3vars.secretKey }}',
              'hostname'   => '{{ s3vars.hostname }}',
              'use_ssl'    => true,
              'use_path_style' => true
            )
          )
        );
    {%- if not config['nextcloud']['redisEnable'] %}
    external-redis.config.php: |-
        <?php
        $CONFIG = array (
          'memcache.distributed' => '\OC\Memcache\Redis',
          'memcache.locking' => '\OC\Memcache\Redis',
          'redis' => array(
            'host' => '{{ config["nextcloud"]["redis"]["host"] }}',
            'password' => '{{ config["nextcloud"]["redis"]["password"] }}',
          )
        );
        $CONFIG['redis']['port'] = (int) {{ config["nextcloud"]["redis"]["port"] }};
    {%- endif %}


  # For example, to use S3 as primary storage
  # ref: https://docs.nextcloud.com/server/13/admin_manual/configuration_files/primary_storage.html#simple-storage-service-s3
  #
  #  configs:
  #    s3.config.php: |-
  #      <?php
  #      $CONFIG = array (
  #        'objectstore' => array(
  #          'class' => '\\OC\\Files\\ObjectStore\\S3',
  #          'arguments' => array(
  #            'bucket'     => 'my-bucket',
  #            'autocreate' => true,
  #            'key'        => 'xxx',
  #            'secret'     => 'xxx',
  #            'region'     => 'us-east-1',
  #            'use_ssl'    => true
  #          )
  #        )
  #      );



internalDatabase:
  {%- if dbvars.type == "subchart" %}
  enabled: true
  name: nextcloud
  {%- else %}
  enabled: false
  {%- endif %}

##
## External database configuration
##
externalDatabase:
  {%- if dbvars.type in ['external', 'socle' ] %}
  enabled: true

  ## Supported database engines: mysql or postgresql
  type: {{ dbvars.provider }}

  ## Database host
  host: {{ dbvars.host}}:{{ dbvars.port }}

  ## Database user
  user: {{ dbvars.user }}

  ## Database password
  password: {{ dbvars.password | tojson }}

  ## Database name
  database: {{ dbvars.dbname }}

  ## Use a existing secret
  existingSecret:
    enabled: false
    # secretName: nameofsecret
    # usernameKey: username
    # passwordKey: password
  {%- else %}
  enabled: false
  {%- endif %}

##
## MariaDB chart configuration
##
mariadb:
  ## Whether to deploy a mariadb server to satisfy the applications database requirements. To use an external database set this to false and configure the externalDatabase parameters
  enabled: false

##
## PostgreSQL chart configuration
## for more options see https://github.com/bitnami/charts/tree/master/bitnami/postgresql
##
postgresql:
  {%- if dbvars.type == "subchart" %}
  enabled: true
  global:
    postgresql:
      auth:
        username: {{ dbvars.user }}
        password: {{ dbvars.password | tojson }}
        database: {{ dbvars.dbname }}
  primary:
    persistence:
      {%- if config['nextcloud']['dbpersistence'] %}
      enabled: true
      # storageClass: ""
      accessMode: ReadWriteOnce
      size: {{ config['nextcloud']['dbvolumesize'] }}
      {%- else %}
      enabled: false
      {%- endif %}
  {%- else %}
  enabled: false
  {%- endif %}

##
## Redis chart configuration
## for more options see https://github.com/bitnami/charts/tree/master/bitnami/redis
##

redis:
  {%- if config['nextcloud']['redisEnable'] %}
  enabled: true
  auth:
    enabled: true
    password: {{ config['nextcloud']['redisAuthPassword'] }}
  {%- else %}
  enabled: false
  {%- endif %}

## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  {%- if config['default']['demoMode'] %}
  enabled: false
  {%- else %}
  enabled: true
  accessMode: ReadWriteOnce
  size: {{ config['nextcloud']['persistenceVolumeSize'] }}
  {%- endif %}

resources:
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  requests:
    cpu: "{{ get_value(config, 'nextcloud', 'resources:requests:cpu') }}"
    memory: "{{ get_value(config, 'nextcloud', 'resources:requests:memory') }}"
  limits:
    cpu: "{{ get_value(config, 'nextcloud', 'resources:limits:cpu') }}"
    memory: "{{ get_value(config, 'nextcloud', 'resources:limits:memory') }}"

## Enable pod autoscaling using HorizontalPodAutoscaler
## ref: https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/
##
hpa:
  cputhreshold: {{ get_value(config, 'nextcloud', 'autoscaling:cpuAverageUtilization') }}
  enabled: {{ get_value(config, 'nextcloud', 'autoscaling:enabled')}}
  # The minimum and maximum number of replicas for the Agenda Deployment
  minPods: {{ get_value(config, 'nextcloud', 'autoscaling:minReplicas') }}
  maxPods: {{ get_value(config, 'nextcloud', 'autoscaling:maxReplicas') }}
