{%- if get_value(config, 'shlink', 'database:provider') == 'mariadb' %}
{%- from "include/mariadb/restore-script.j2" import mariadb_restore_script as restore_script -%}
{%- else %}
{%- from "include/postgresql/restore-script.j2" import postgresql_restore_script as restore_script -%}
{%- endif %}

#!/bin/bash

{{ restore_script(config, 'shlink', 'deployment') }}
