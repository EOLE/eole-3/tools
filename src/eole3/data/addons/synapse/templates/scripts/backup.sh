{%- from "include/postgresql/backup-script.j2" import postgresql_backup_script as backup_script -%}
{%- from "include/s3/backup-script.j2" import s3_backup_script -%}

#!/bin/bash

{{ backup_script(config, 'synapse') }}
{{ s3_backup_script(config, 'synapse') }}
