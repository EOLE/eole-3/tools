{%- from "include/postgresql/restore-script.j2" import postgresql_restore_script as restore_script -%}
{%- from "include/s3/restore-script.j2" import s3_restore_script -%}

#!/bin/bash

{{ restore_script(config, 'synapse', 'deployment') }}
{{ s3_restore_script(config, 'synapse') }}
