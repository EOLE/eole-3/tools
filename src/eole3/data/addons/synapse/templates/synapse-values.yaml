{%- from "include/postgresql/dbvars.inc" import load_dbvars -%}
{%- set dbvars = load_dbvars(config=config, component='synapse') | from_json -%}

{%- from 'include/utils/s3-vars.j2' import load_s3vars %}
{%- set s3vars = load_s3vars(config, 'synapse') | from_json -%}

{%- if get_value(config, "synapse", "images:registry") != "upstream" %}
image:
  registry: {{ get_value(config, "synapse", "images:registry") }}
  repository: {{ get_value(config, "synapse", "images:synapse:name") }}
  {%- if get_value(config, "synapse", "images:synapse:tag") %}
  tag: {{ get_value(config, "synapse", "images:synapse:tag") }}
  {%- endif %}
{%- endif %}

generalConfig:
  serverName: {{ config['synapse']['hostname'] }}.{{ config['default']['domain'] }}
  publicRoomsFederation: {{ config['synapse']['publicRoomsFederation'] }}
  defaultE2ee: {{ config['synapse']['defaultE2ee'] }}
  {%- if config['synapse']['autoJoinRooms'].length != 0 %}
  autoJoinRooms:
    {%- for room in config['synapse']['autoJoinRooms'] %}
    - {{ room }}
    {%- endfor %}
  {%- endif %}
database:
  type: psycopg2
  host: {{ dbvars.host }}
  port: {{ dbvars.port }}
  username: {{ dbvars.user }}
  password: {{ dbvars.password }}
  name: {{ dbvars.dbname }}

s3:
  bucket: {{ s3vars.buckets | first }}
  regionName: ""
  endpointUrl: https://{{ s3vars.hostname }}
  keyId: {{ s3vars.accessKey }}
  secretAccessKey: {{ s3vars.secretKey }}

oidc:
  clientId: {{ config['synapse']['keycloak']['clientName'] }}
  clientSecret: {{ config['synapse']['keycloak']['clientSecret'] }}
  idpName: {{ config['synapse']['keycloak']['idpName'] }}
  issuer: https://{{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}/auth/realms/{{ config['keycloak']['realm'] }}

ingress:
  enabled: true
  className: {{ get_value(config, 'synapse', 'ingress:className') }}
  annotations:
  {%- if config['cert-manager']['enabled'] %}
    cert-manager.io/cluster-issuer: "{{ config['cert-manager']['clusterIssuerName']|default('letsencrypt', true) }}-{{ config['cert-manager']['type'] }}"
  {%- endif %}
  hosts:
    - host: {{ config['synapse']['hostname'] }}.{{ config['default']['domain'] }}
      paths:
        - path: /_synapse
          pathType: ImplementationSpecific
        - path: /_matrix
          pathType: ImplementationSpecific
        - path: /.well-known
          pathType: ImplementationSpecific
  tls:
    - hosts:
      - {{ config['synapse']['hostname'] }}.{{ config['default']['domain'] }}
      {%- if config['cert-manager']['enabled'] %}
      secretName: tls-{{ config['synapse']['hostname'] }}
      {%- endif %}

autoscaling:
  enabled: {{ get_value(config, 'synapse', 'autoscaling:enabled') }}
  # The minimum and maximum number of replicas for the Agenda Deployment
  minReplicas: {{ get_value(config, 'synapse', 'autoscaling:minReplicas') }}
  maxReplicas: {{ get_value(config, 'synapse', 'autoscaling:maxReplicas') }}
  # The metrics to use for scaling
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: {{ get_value(config, 'synapse', 'autoscaling:cpuAverageUtilization') }}
  behavior:
    scaleDown:
      stabilizationWindowSeconds: 300
      policies:
      - type: Pods
        value: 1
        periodSeconds: 300

resources:
  requests:
    cpu: "{{ get_value(config, 'synapse', 'resources:requests:cpu') }}"
    memory: "{{ get_value(config, 'synapse', 'resources:requests:memory') }}"
  limits:
    cpu: "{{ get_value(config, 'synapse', 'resources:limits:cpu') }}"
    memory: "{{ get_value(config, 'synapse', 'resources:limits:memory') }}"
