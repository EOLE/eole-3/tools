{%- from "include/s3/restore-script.j2" import s3_restore_script -%}

#!/bin/bash

{{ s3_restore_script(config, 'loki') }}
