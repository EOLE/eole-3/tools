{%- from "include/postgresql/restore-script.j2" import postgresql_restore_script -%}

#!/bin/bash

[[ -z $1 ]] && echo "Please choose a backup day" && exit 1

{{ postgresql_restore_script(config, 'keycloak', 'statefulsets') }}
