{%- from "include/postgresql/restore-script.j2" import postgresql_restore_script -%}

#!/bin/bash

{{ postgresql_restore_script(config, 'digiflashcards', 'deployment') }}
