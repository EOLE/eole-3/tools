{%- from "include/postgresql/backup-script.j2" import postgresql_backup_script -%}

#!/bin/bash

{{ postgresql_backup_script(config, 'digistrip') }}
