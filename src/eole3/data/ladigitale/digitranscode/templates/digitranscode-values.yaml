{%- if get_value(config, "digitranscode", "images:registry") != "upstream" %}
image:
  repository: {{ get_value(config, "digitranscode", "images:registry") }}/{{ get_value(config, "digitranscode", "images:digitranscode:name") }}
  {%- if get_value(config, "digitranscode", "images:digitranscode:tag") %}
  tag: {{ get_value(config, "digitranscode", "images:digitranscode:tag") }}
  {%- endif %}
  pullPolicy: Always
{%- endif %}

replicaCount: {{ get_value(config, 'digitranscode', 'autoscaling:minReplicas') }}

ingress:
  enabled: true
  className: {{ get_value(config, 'digitranscode', 'ingress:className') }}
  annotations:
    nginx.ingress.kubernetes.io/configuration-snippet: |
      more_set_headers "Cross-Origin-Opener-Policy: same-origin";
      more_set_headers "Cross-Origin-Embedder-Policy: require-corp";
  {%- if config['cert-manager']['enabled'] %}
    cert-manager.io/cluster-issuer: "{{ config['cert-manager']['clusterIssuerName']|default('letsencrypt', true) }}-{{ config['cert-manager']['type'] }}"
  {%- endif %}
  hosts:
    - host: {{ config['digitranscode']['hostname'] }}.{{ config['default']['domain'] }}
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
    - hosts:
      - {{ config['digitranscode']['hostname'] }}.{{ config['default']['domain'] }}
      {%- if config['cert-manager']['enabled'] %}
      secretName: tls-{{ config['digitranscode']['hostname'] }}
      {%- endif %}

autoscaling:
  enabled: {{ get_value(config, 'digitranscode', 'autoscaling:enabled') }}
  # The minimum and maximum number of replicas for the Agenda Deployment
  minReplicas: {{ get_value(config, 'digitranscode', 'autoscaling:minReplicas') }}
  maxReplicas: {{ get_value(config, 'digitranscode', 'autoscaling:maxReplicas') }}
  # The metrics to use for scaling
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: {{ get_value(config, 'digitranscode', 'autoscaling:cpuAverageUtilization') }}
  behavior:
    scaleDown:
      stabilizationWindowSeconds: 300
      policies:
      - type: Pods
        value: 1
        periodSeconds: 300

resources:
  requests:
    cpu: "{{ get_value(config, 'digitranscode', 'resources:requests:cpu') }}"
    memory: "{{ get_value(config, 'digitranscode', 'resources:requests:memory') }}"
  limits:
    cpu: "{{ get_value(config, 'digitranscode', 'resources:limits:cpu') }}"
    memory: "{{ get_value(config, 'digitranscode', 'resources:limits:memory') }}"
