{% from "include/mongodb/dbvars.inc" import load_dbvars %}
{%- set dbvars = load_dbvars(config=config, component="laboite") | from_json -%}
# Mongodb parameters to connect to
mongo:
  host: {{ dbvars.host }}
  user: {{ dbvars.user }}
  password: {{ dbvars.password }}
  rsName: {{ dbvars.replicaset }}
  database: {{ dbvars.dbname }}
  port: "{{ dbvars.port }}"
