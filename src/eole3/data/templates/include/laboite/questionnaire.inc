# CronJob for questionnaire
cron:
{%- if config['questionnaire']['cronEnabled'] %}
  enabled: true
  schedule: {{ config['questionnaire']['cronSchedule'] }}
  tag: {{ config['questionnaire']['cronTag'] }}
  script: {{ config['questionnaire']['cronScript'] }}
{% else %}
  enabled: false
{%- endif %}
