{% from "include/mongodb/dbvars.inc" import load_dbvars %}
{%- set dbvars = load_dbvars(config=config, component="laboite") | from_json -%}
# Parameters to connect to mongodb database
mongoName: {{ dbvars.host }}
mongoPort: "{{ dbvars.port }}"
mongoRsname: "{{ dbvars.replicaset }}"
mongoDatabase: "{{ dbvars.dbname }}"
mongoUsername: "{{ dbvars.user }}"
mongoPassword: "{{ dbvars.password | urlencode }}"

# Keycloak parameters
keycloak_url: {{ config['keycloak']['hostname'] }}.{{ config['default']['domain'] }}
keycloak_realm: {{ config['keycloak']['realm'] }}

