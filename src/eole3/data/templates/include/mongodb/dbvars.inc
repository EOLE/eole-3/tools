{% macro load_dbvars(config=None, component=None) %}
{%- set dbvars = {
      'user': config[component]['database']['user'],
      'password': config[component]['database']['password'],
      'dbname': config[component]['database']['dbname'],
      'type': get_value(config, component, 'database:type'),
      'provider': get_value(config, component, 'database:provider')
    } %}
{%- set provider_config = config[dbvars.provider] %}

{%- if dbvars.type == "socle" and not provider_config.deploy %}
    {%- raise("'"
                ~ component
                ~ ".database.type' can't be 'socle' when '"
                ~ dbvars.provider
                ~ ".deploy' is False") %}
{%- elif dbvars.type == "socle" %}
  {%- do dbvars.update({'host': provider_config.hostname ~ "." ~ get_value(config, dbvars.provider, 'namespace')}) %}
  {%- do dbvars.update({'replicaset': provider_config.rsName}) %}
  {%- do dbvars.update({'port': provider_config.port}) %}
  {%- do dbvars.update({'adminuser': 'root'}) %}
  {%- do dbvars.update({'adminpassword': provider_config.admin.password}) %}
  {%- do dbvars.update({'admindbname': 'mongodb'}) %}
{%- endif -%}
{{ dbvars | tojson }}
{% endmacro %}
