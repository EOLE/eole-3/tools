{% macro load_dbvars(config=None, component=None) %}
{%- set dbvars = {
      'user': config[component]['database']['user'],
      'password': config[component]['database']['password'],
      'dbname': config[component]['database']['dbname'],
      'type': get_value(config, component, 'database:type'),
      'provider': get_value(config, component, 'database:provider'),
      'options': get_value(config, component, 'database:options')
    } %}
{%- set provider_config = config[dbvars.provider] %}

{%- if dbvars.type == "socle" and not provider_config.deploy %}
    {%- raise("'"
                ~ component
                ~ ".database.type' can't be 'socle' when '"
                ~ dbvars.provider
                ~ ".deploy' is False") %}
{%- elif dbvars.type == "socle" %}
  {%- do dbvars.update({'host': provider_config.hostname ~ "." ~ get_value(config, dbvars.provider, 'namespace')}) %}
  {%- do dbvars.update({'port': provider_config.port}) %}
  {%- do dbvars.update({'adminuser': 'postgres'}) %}
  {%- do dbvars.update({'adminpassword': provider_config.admin.password}) %}
  {%- do dbvars.update({'admindbname': 'postgres'}) %}
{%- elif dbvars.type == "external" %}
  {%- do dbvars.update({'host': get_value(config, component, 'database:hostname')}) %}
  {%- do dbvars.update({'port': get_value(config, component, 'database:port')}) %}
  {%- do dbvars.update({'adminuser': get_value(config, component, 'database:admin:user')}) %}
  {%- do dbvars.update({'adminpassword': get_value(config, component, 'database:admin:password')}) %}
  {%- do dbvars.update({'admindbname': get_value(config, component, 'database:admin:dbname')}) %}
{%- elif dbvars.type == "subchart" %}
  {%- do dbvars.update({'host': component ~ "-" ~ dbvars.provider ~ "." ~ get_value(config, component, 'namespace')}) %}
  {%- do dbvars.update({'port': config[component]['database']['port']}) %}
  {%- do dbvars.update({'adminuser': config[component]['database']['admin']['user']}) %}
  {%- do dbvars.update({'adminpassword': config[component]['database']['admin']['password']}) %}
  {%- do dbvars.update({'admindbname': config[component]['database']['admin']['dbname']}) %}
{%- endif -%}
{{ dbvars | tojson }}
{% endmacro %}
