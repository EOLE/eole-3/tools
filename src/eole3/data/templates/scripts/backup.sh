{%- from "include/mongodb/backup-script.j2" import mongodb_backup_script -%}
{%- from "include/s3/backup-script.j2" import s3_backup_script -%}

#!/bin/bash

{% include 'include/utils/log.sh.j2' %}

{{ mongodb_backup_script(config, 'laboite') }}

cd infra/keycloak/
bash backup.sh
cd - > /dev/null

{{ s3_backup_script(config, 'laboite') }}
