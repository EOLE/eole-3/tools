{%- from "include/mongodb/list-backup-script.j2" import list_mongodb_backup_script -%}
{%- from "include/s3/list-backup-script.j2" import list_s3_backup_script -%}
#!/bin/bash

{{ list_mongodb_backup_script(config, 'laboite') }}

cd infra/keycloak/
bash list-backup.sh
cd - 1>/dev/null

{{ list_s3_backup_script(config, 'laboite') }}
