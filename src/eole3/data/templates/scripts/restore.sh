{%- from "include/mongodb/restore-script.j2" import mongodb_restore_script -%}
{%- from "include/s3/restore-script.j2" import s3_restore_script -%}

#!/bin/bash

[[ -z $1 ]] && echo "Please choose a backup day" && exit 1

{% include 'include/utils/log.sh.j2' %}

{{ mongodb_restore_script(config, 'laboite', 'deployments') }}

cd infra/keycloak/
bash restore.sh $1
cd - > /dev/null

{{ s3_restore_script(config, 'laboite') }}
