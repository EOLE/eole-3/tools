# -*- coding: utf-8 -*-

from . import data
import click
import functools

default_output = "./install"
default_output_file = click.get_text_stream("stdout")


def component(type):
    def inner(func):
        @click.option(
            "-n",
            "--name",
            type=click.Choice(data.components[type]),
            required=True,
            help=f"{type.title()} name",
        )
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return inner


def socle_component():
    def inner(func):
        @click.option(
            "-n",
            "--name",
            type=click.Choice(data.socle_components),
            default="all",
            required=False,
            help=f"Socle name",
        )
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return inner


def maintenance_component(type):
    def inner(func):
        @click.option(
            "-n",
            "--name",
            type=click.Choice(data.maintenance_components[type]),
            required=True,
            help=f"{type.title()} name",
        )
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return inner


def config(func):
    @click.option(
        "-c",
        "--config",
        type=click.Path(exists=True),
        multiple=True,
        help=f"Configuration value file",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


def backup_actions(func):
    @click.option(
        "-a",
        "--action",
        type=click.Choice(["list", "save"]),
        required=True,
        help=f"Actions for backup command",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


def restore_day(func):
    @click.option(
        "-d",
        "--day",
        type=click.Choice(
            [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday",
            ]
        ),
        required=True,
        help=f"Backup day",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


def output_dir(func):
    @click.option(
        "-o",
        "--outputdir",
        "outputdir",
        default=default_output,
        help=f"Output Directory (default: {default_output})",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


def output_file(func):
    @click.option(
        "-o",
        "--outputfile",
        "outputfile",
        type=click.File(mode="w"),
        default=default_output_file,
        help=f"Output File (default: stdout)",
    )
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper
