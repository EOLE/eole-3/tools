# -*- coding: utf-8 -*-

import json
import yaml
import sys
from deepmerge import Merger

from jinja2 import nodes
from jinja2.ext import Extension
from jinja2.exceptions import TemplateRuntimeError
from shutil import which


def check_commands():
    """
    Check required commands
    """
    commands = ["jq", "curl", "kubectl", "helm"]
    for command in commands:
        if not which(command):
            print("{} command not found".format(command))
            sys.exit(1)


def merge_config(config_files=None):
    config = {}
    merger = Merger(
        [(list, ["override"]), (dict, ["merge"]), (set, ["union"])],
        ["override"],
        ["override"],
    )
    for file in config_files:
        config = merger.merge(
            config, yaml.load(open(file, "r"), Loader=yaml.FullLoader)
        )
    return config


def get_value(config, component, relative_path):
    """
    Return the value of a component relative path
    If not exists, return the default relative path value

    - config: the entire config dictionnary
    - component: string with the component name
    - relative_path: string with the relative path (ex.: "database:userName")
    """
    relative_path = relative_path.split(":")
    value = _search(config[component], relative_path)
    if value is None:
        value = _search(config["default"], relative_path)
    return value


def _search(component_config, relative_path):
    """
    Return the value of a component relative path

    - component_config: component subdictionnary
    - relative_path: list of dictionnary keys
    """
    if len(relative_path) > 0:
        if relative_path[0] in component_config.keys():
            return _search(component_config[relative_path[0]], relative_path[1:])
        else:
            return None
    else:
        return component_config


def from_json(data):
    return json.loads(data)


class RaiseExtension(Extension):
    # This is our keyword(s):
    tags = {"raise"}

    def __init__(self, environment):
        super().__init__(environment)

        # add the defaults to the environment
        environment.extend(fragment_cache_prefix="", fragment_cache=None)

    # See also: jinja2.parser.parse_include()
    def parse(self, parser):
        # the first token is the token that started the tag. In our case we
        # only listen to "raise" so this will be a name token with
        # "raise" as value. We get the line number so that we can give
        # that line number to the nodes we insert.
        lineno = next(parser.stream).lineno

        # Extract the message from the template
        message_node = parser.parse_expression()

        return nodes.CallBlock(
            self.call_method("_raise", [message_node], lineno=lineno),
            [],
            [],
            [],
            lineno=lineno,
        )

    def _raise(self, msg, caller):
        raise TemplateRuntimeError(msg)
